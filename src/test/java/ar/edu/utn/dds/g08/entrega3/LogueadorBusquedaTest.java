package ar.edu.utn.dds.g08.entrega3;

import static org.mockito.Mockito.*;

import org.junit.Test;

import ar.edu.utn.dds.g08.busquedas.*;
import ar.edu.utn.dds.g08.datos.*;
import ar.edu.utn.dds.g08.perfilesUsuarios.*;

public class LogueadorBusquedaTest {
	BuscadorPDIs repositorio = new BuscadorRepositorio(
			new CreadorRepositorioPDI().buildRepositorio());

	@Test
	public void testLogueador() {
		LogueadorBusqueda logueador = new LogueadorBusqueda();
		LogueadorBusqueda espiaLog = spy(logueador);

		CreadorPerfilTerminal builderTerminal = new CreadorPerfilTerminal();

		builderTerminal.setRepositorio(repositorio);
		builderTerminal.setLogueador(espiaLog);

		PerfilTerminal terminal = builderTerminal.buildTerminalLogueador();

		terminal.buscarPDI("101");

		verify(espiaLog, times(1)).actualizar(any());

		terminal.buscarPDI("hola");
		terminal.buscarPDI("Francés");

		verify(espiaLog, times(3)).actualizar(any());
	}
}
