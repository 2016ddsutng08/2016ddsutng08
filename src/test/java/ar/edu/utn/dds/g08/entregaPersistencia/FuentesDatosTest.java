package ar.edu.utn.dds.g08.entregaPersistencia;

import static org.junit.Assert.*;

import org.junit.Test;

import ar.edu.utn.dds.g08.busquedas.*;
import ar.edu.utn.dds.g08.datos.*;
import ar.edu.utn.dds.g08.dominio.*;
import ar.edu.utn.dds.g08.fuentesDatos.*;
import ar.edu.utn.dds.g08.entrega2.*;

import java.util.*;

public class FuentesDatosTest extends PersistenciaTest {

	@Test
	public void testRepositorioPDI() {
		RepositorioPDI repositorio = new CreadorRepositorioPDI()
				.buildRepositorio();

		// 10 locales 10 bancos 10 coles 8 paradas 3 cgp
		assertTrue(repositorio.stream().count() == 41);

		withTransaction(() -> {
			repositorio.stream().filter(p -> p.getClass() == Parada.class)
					.forEach(p -> this.sesion.persist(p));
			repositorio.stream().filter(p -> p.getClass() != Parada.class)
					.forEach(p -> this.sesion.persist(p));
		});

		this.sesion.clear();

		RepositorioPDI repositorio2 = new RepositorioPDI();

		this.sesion.createQuery("from PuntoDeInteres", PuntoDeInteres.class)
				.getResultList().forEach(p -> repositorio2.putPDI(p));

		assertTrue(repositorio2.stream().count() == 41);

		withTransaction(() -> {
			repositorio2.stream().forEach(p -> this.sesion.delete(p));
		});

		RepositorioPDI repositorio3 = new RepositorioPDI();

		this.sesion.createQuery("from PuntoDeInteres", PuntoDeInteres.class)
				.getResultList().forEach(p -> repositorio3.putPDI(p));

		assertTrue(repositorio3.stream().count() == 0);

	}

	@Test
	public void testRepositorioExtendido() {
		RepositorioPDI repositorio = new CreadorRepositorioPDI()
				.buildRepositorio();

		withTransaction(() -> {
			repositorio.stream().filter(p -> p.getClass() == Parada.class)
					.forEach(p -> this.sesion.persist(p));
			repositorio.stream().filter(p -> p.getClass() != Parada.class)
					.forEach(p -> this.sesion.persist(p));
		});

		this.sesion.clear();

		RepositorioPDI repositorio2 = new RepositorioPDI();

		this.sesion.createQuery("from PuntoDeInteres", PuntoDeInteres.class)
				.getResultList().forEach(p -> repositorio2.putPDI(p));

		BuscadorPDIs buscadorRepo = new BuscadorRepositorio(repositorio2);

		assertTrue(buscadorRepo.buscarPDI("Santander", "depósitos").size() == 0);
		assertTrue(buscadorRepo.buscarPDI("ICBC").size() == 2);

		BuscadorPDIs buscadorBanco = new BuscadorBancos(
				new ConsultaBancosTest.OrigenBanco());

		BuscadorPDIs extendido = new BuscadorExtendido(Arrays.asList(
				buscadorRepo, buscadorBanco));

		assertTrue(extendido.buscarPDI("Santander", "depósitos").size() == 4);
		assertTrue(extendido.buscarPDI("ICBC", "depósitos").size() == 4);

		withTransaction(() -> {
			repositorio2.stream().forEach(p -> this.sesion.delete(p));
		});

	}
}
