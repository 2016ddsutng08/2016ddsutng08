package ar.edu.utn.dds.g08.entregaPersistencia;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Before;

import ar.edu.utn.dds.g08.persistencia.SessionFactorySingleton;

public class PersistenciaTest {
	protected SessionFactory factory = SessionFactorySingleton.getInstance();

	protected Session sesion;

	@Before
	public void setUp() {
		this.sesion = factory.openSession();
	}

	@After
	public void setOff() {
		this.sesion.close();
	}

	public void withTransaction(Runnable command) {
		Transaction transaction = this.sesion.beginTransaction();
		command.run();
		transaction.commit();
	}

}
