package ar.edu.utn.dds.g08.entregaPersistencia;

import java.util.Arrays;

import org.junit.*;

import static org.junit.Assert.*;

import ar.edu.utn.dds.g08.acciones.*;
import ar.edu.utn.dds.g08.datos.CreadorPerfilTerminal;
import ar.edu.utn.dds.g08.datos.CreadorRepositorioPDI;
import ar.edu.utn.dds.g08.dominio.Parada;
import ar.edu.utn.dds.g08.dominio.PuntoDeInteres;
import ar.edu.utn.dds.g08.entrega4.BajaPDIsInactivosTest.OrigenBaja;
import ar.edu.utn.dds.g08.fuentesDatos.RepositorioPDI;
import ar.edu.utn.dds.g08.perfilesUsuarios.LogueadorBusqueda;
import ar.edu.utn.dds.g08.perfilesUsuarios.ObservadorBusqueda;
import ar.edu.utn.dds.g08.perfilesUsuarios.PerfilAdministrador;
import ar.edu.utn.dds.g08.perfilesUsuarios.PerfilTerminal;

public class AccionesTest extends PersistenciaTest {

	// @Test tengo qeu ver comopersistir los observadores
	public void testAccionesUsuarios() {
		PerfilTerminal terminal = new CreadorPerfilTerminal()
				.buildTerminalVacio();

		assertTrue(terminal.getObservadores().size() == 0);

		withTransaction(() -> {
			this.sesion.persist(terminal);
		});

		this.sesion.clear();

		PerfilTerminal terminal2 = this.sesion.find(PerfilTerminal.class,
				terminal.getNombreUsuario());

		assertTrue(terminal2.getObservadores().size() == 0);

		PerfilAdministrador administrador = new PerfilAdministrador(
				"Administrador");

		AccionesUsuarios accionActualizar = new AccionesUsuarios();
		NotificadorVacio notificador = new NotificadorVacio();
		ObservadorBusqueda logueador = new LogueadorBusqueda();

		accionActualizar.setNotificador(notificador);
		accionActualizar.addAccion(terminal2, Arrays.asList(logueador));

		withTransaction(() -> {
			EstadoProceso estado = administrador
					.ejecutarProceso(accionActualizar);

			while (!estado.estaFinalizado())
				System.out.println("todavía no terminó");

			assertTrue(estado.finalizadoCorrectamente());
		});

		PerfilTerminal terminal3 = this.sesion.find(PerfilTerminal.class,
				terminal.getNombreUsuario());

		assertTrue(terminal3.getObservadores().size() == 1);

	}

	@Test
	public void testActualizacionLocalesComerciales() {
		RepositorioPDI repositorio = new CreadorRepositorioPDI()
				.buildRepositorio();

		withTransaction(() -> {
			repositorio.stream().filter(p -> p.getClass() == Parada.class)
					.forEach(p -> this.sesion.persist(p));
			repositorio.stream().filter(p -> p.getClass() != Parada.class)
					.forEach(p -> this.sesion.persist(p));
		});

		this.sesion.clear();

		RepositorioPDI repositorio2 = new RepositorioPDI();

		this.sesion.createQuery("from PuntoDeInteres", PuntoDeInteres.class)
				.getResultList().forEach(p -> repositorio2.putPDI(p));

		PerfilAdministrador administrador = new PerfilAdministrador(
				"Administrador");

		Accion accionActualizar = new ActualizacionLocalesComerciales(
				"/home/dds/git/2016ddsutng08/src/test/resources/actualizacionLocalesComercialesTest.txt",
				repositorio2);

		accionActualizar.setNotificador(new NotificadorVacio());

		assertTrue(repositorio2.buscarPDI("Levis").size() == 0);
		assertTrue(repositorio2.buscarPDI("Chalina").size() == 0);

		EstadoProceso estado = administrador.ejecutarProceso(accionActualizar);

		while (!estado.estaFinalizado())
			System.out.println("todavía no terminó");

		assertTrue(estado.finalizadoCorrectamente());

		withTransaction(() -> {
			repositorio2.stream().filter(p -> p.getClass() == Parada.class)
					.forEach(p -> this.sesion.persist(p));
			repositorio2.stream().filter(p -> p.getClass() != Parada.class)
					.forEach(p -> this.sesion.persist(p));
		});

		this.sesion.clear();

		RepositorioPDI repositorio3 = new RepositorioPDI();

		this.sesion.createQuery("from PuntoDeInteres", PuntoDeInteres.class)
				.getResultList().forEach(p -> repositorio3.putPDI(p));

		assertTrue(repositorio3.buscarPDI("Levis").size() == 1);
		assertTrue(repositorio3.buscarPDI("Chalina").size() == 1);
		assertTrue(repositorio3.stream().count() == 42);

		withTransaction(() -> {
			repositorio3.stream().forEach(p -> this.sesion.delete(p));
		});

	}

	@Test
	public void testBajaPDIsInactivos() {
		RepositorioPDI repositorio = new CreadorRepositorioPDI()
				.buildRepositorio();

		assertTrue(repositorio.buscarPDI("114").size() == 1);

		withTransaction(() -> {
			repositorio.stream().filter(p -> p.getClass() == Parada.class)
					.forEach(p -> this.sesion.persist(p));
			repositorio.stream().filter(p -> p.getClass() != Parada.class)
					.forEach(p -> this.sesion.persist(p));
		});

		this.sesion.clear();

		RepositorioPDI repositorio2 = new RepositorioPDI();

		this.sesion.createQuery("from PuntoDeInteres", PuntoDeInteres.class)
				.getResultList().forEach(p -> repositorio2.putPDI(p));

		PerfilAdministrador administrador = new PerfilAdministrador(
				"Administrador");

		ActualizacionPDIsActivos interfazGobBAOK = new OrigenBaja();
		Notificador notificador = new NotificadorVacio();
		BajaPDIsInactivos accionBaja = new BajaPDIsInactivos(interfazGobBAOK,
				repositorio2);
		accionBaja.setNotificador(notificador);

		assertTrue(repositorio2.buscarPDI("Comuna 01").size() == 1);
		assertTrue(repositorio2.buscarPDI("114").size() == 3);
		assertTrue(repositorio2.buscarPDI("Herencia").size() == 1);
		assertTrue(repositorio2.buscarPDI("Galicia cheto").size() == 1);

		EstadoProceso estado = administrador.ejecutarProceso(accionBaja);

		while (!estado.estaFinalizado())
			System.out.println("todavía no terminó");

		assertTrue(estado.finalizadoCorrectamente());

		withTransaction(() -> {
			accionBaja.getPDIsEliminados().forEach(p -> this.sesion.delete(p));
		});

		RepositorioPDI repositorio3 = new RepositorioPDI();

		this.sesion.createQuery("from PuntoDeInteres", PuntoDeInteres.class)
				.getResultList().forEach(p -> repositorio3.putPDI(p));

		assertTrue(repositorio3.buscarPDI("Comuna 01").size() == 0);
		assertTrue(repositorio3.buscarPDI("114").size() == 0);
		assertTrue(repositorio3.buscarPDI("Herencia").size() == 0);
		assertTrue(repositorio3.buscarPDI("Galicia cheto").size() == 0);
		assertTrue(35 == repositorio3.stream().count());

		withTransaction(() -> {
			repositorio3.stream().forEach(p -> this.sesion.delete(p));
		});
	}

}