package ar.edu.utn.dds.g08.entrega2;

import java.util.Arrays;

import ar.edu.utn.dds.g08.fuentesDatos.*;
import ar.edu.utn.dds.g08.dominio.*;
import ar.edu.utn.dds.g08.excepciones.*;
import ar.edu.utn.dds.g08.datos.*;
import ar.edu.utn.dds.g08.valueClasses.*;

import org.junit.Test;

import static org.junit.Assert.*;

public class RepositorioTest {
	RepositorioPDI repositorio = new CreadorRepositorioPDI().buildRepositorio();

	@Test
	public void testAlta() {
		// 10 locales 10 bancos 10 coles 8 paradas 3 cgp
		assertTrue(repositorio.stream().count() == 41);
	}

	@Test(expected = DuplicadoException.class)
	public void testAltaException() {
		// en el caso de CGP el id es la comuna, asi que voy a tomar
		// un CGP de la entrega 1 con = comuna y lo intentaré agregar
		repositorio.putPDI(new CreadorCGP().buildCGP2());
	}

	@Test
	public void testConsulta() {
		Parada parada1 = new Parada("icono parada", new Direccion(
				new PointAdaptado(-34.657932, -58.458362)));
		Parada parada2 = new Parada("icono parada", new Direccion(
				new PointAdaptado(-34.657932, -58.458362)));

		int id = repositorio.putPDI(parada1);

		assertTrue(parada1.equals(repositorio.buscarPDI(id)));
		assertTrue(parada2.equals(repositorio.buscarPDI(id)));

		assertTrue(repositorio.buscarPDI(150) == null);

		Colectivo bondi = new CreadorColectivo().buildColectivo6();

		assertTrue(repositorio.buscarPDI("60").contains(bondi));
		assertTrue(repositorio.buscarPDIPorTipo("60", Colectivo.class)
				.contains(bondi));
		assertTrue(repositorio.buscarPDI("510").isEmpty());
	}

	@Test
	public void testModificacion() {
		Rubro comercioAlim = new Rubro("Alimentos", 0.1);
		Rubro verd = new Rubro("Verduleria", 5);
		Horario entreSem = new Horario(1, 5, 10, 00, 19, 00);
		LocalComercial carniceria1 = new LocalComercial("icono", new Direccion(
				new PointAdaptado(-34.514562, -58.742641)), comercioAlim,
				Arrays.asList("Bondiola", "Pollo", "Vacío"),
				Arrays.asList(entreSem), "Lo de Maxi");
		LocalComercial carniceria2 = new LocalComercial("icono", new Direccion(
				new PointAdaptado(-34.514562, -58.742641)), comercioAlim,
				Arrays.asList("Bondiola", "Pollo", "Vacío"),
				Arrays.asList(entreSem), "Lo de Maxi");

		int id = repositorio.putPDI(carniceria1);

		assertTrue(carniceria2.equals(repositorio.buscarPDI(id)));

		carniceria1 = null;
		carniceria1 = (LocalComercial) repositorio.buscarPDI(id);

		carniceria1.setRubro(verd);
		repositorio.modifyPDI(carniceria1);

		assertFalse(carniceria2.equals(repositorio.buscarPDI(id)));

		assertTrue(carniceria1.equals(repositorio.buscarPDI(id)));
	}

	@Test
	public void testBaja() {
		PuntoDeInteres pdiBaja;
		long count = repositorio.stream().count();

		pdiBaja = repositorio.buscarPDI(14);

		assertTrue(repositorio.contains(pdiBaja));

		repositorio.removePDI(pdiBaja);

		assertFalse(repositorio.contains(pdiBaja));

		assertTrue(--count == repositorio.stream().count());
	}
}
