package ar.edu.utn.dds.g08.entrega1;

import ar.edu.utn.dds.g08.datos.*;
import ar.edu.utn.dds.g08.dominio.*;
import ar.edu.utn.dds.g08.valueClasses.*;

import org.joda.time.Instant;
import org.junit.Test;

import static org.junit.Assert.*;

public class ColectivoTest {
	public Colectivo bondi114 = new CreadorColectivo().buildColectivo1();
	public Colectivo bondi101 = new CreadorColectivo().buildColectivo2();
	public Colectivo bondi152 = new CreadorColectivo().buildColectivo3();
	public Colectivo bondi115 = new CreadorColectivo().buildColectivo4();
	public Colectivo bondi55 = new CreadorColectivo().buildColectivo5();
	public Colectivo bondi60 = new CreadorColectivo().buildColectivo6();
	public Colectivo bondi71 = new CreadorColectivo().buildColectivo7();
	public Colectivo bondi160 = new CreadorColectivo().buildColectivo8();
	public Colectivo bondi7 = new CreadorColectivo().buildColectivo9();
	public Colectivo bondiTotal = new CreadorColectivo().buildColectivo10();

	@Test
	public void testCercania() {
		PointAdaptado puntoCercano = new PointAdaptado(-34.591884, -58.374515); // retiro

		assertTrue(bondi101.esCercano(puntoCercano));
		assertTrue(bondi152.esCercano(puntoCercano));
		assertTrue(bondi7.esCercano(puntoCercano));
		assertTrue(bondiTotal.esCercano(puntoCercano));
		assertTrue(bondi60.esCercano(puntoCercano));
		assertTrue(bondi160.esCercano(puntoCercano));
		assertTrue(bondi115.esCercano(puntoCercano));

		puntoCercano = new PointAdaptado(-34.578453, -58.425047);
		assertTrue(bondi71.esCercano(puntoCercano));
		assertTrue(bondi55.esCercano(puntoCercano));

		puntoCercano = new PointAdaptado(-34.660192, -58.468018);
		assertTrue(bondi114.esCercano(puntoCercano));

		PointAdaptado puntoLejano = new PointAdaptado(-34.636926, -58.478617);
		assertFalse(bondi114.esCercano(puntoLejano));
		assertFalse(bondi101.esCercano(puntoLejano));
		assertFalse(bondi152.esCercano(puntoLejano));
		assertFalse(bondi55.esCercano(puntoLejano));
		assertFalse(bondi115.esCercano(puntoLejano));
		assertFalse(bondi60.esCercano(puntoLejano));
		assertFalse(bondi71.esCercano(puntoLejano));
		assertFalse(bondi160.esCercano(puntoLejano));
		assertFalse(bondi7.esCercano(puntoLejano));
		assertFalse(bondiTotal.esCercano(puntoLejano));

	}

	@Test
	public void testDisponibilidad() {
		// en cualquier horario es verdadero
		Instant horaDisponible = Instant.now();

		assertTrue(bondi114.estaDisponible(horaDisponible));
		assertTrue(bondi101.estaDisponible(horaDisponible));
		assertTrue(bondi152.estaDisponible(horaDisponible));
		assertTrue(bondi115.estaDisponible(horaDisponible));
		assertTrue(bondi55.estaDisponible(horaDisponible));
		assertTrue(bondi60.estaDisponible(horaDisponible));
		assertTrue(bondi160.estaDisponible(horaDisponible));
		assertTrue(bondi7.estaDisponible(horaDisponible));
		assertTrue(bondiTotal.estaDisponible(horaDisponible));
		assertTrue(bondi71.estaDisponible(horaDisponible));

	}

	@Test
	public void testBusqueda() {
		assertTrue(bondi114.estaEnTexto("114"));
		assertFalse(bondi114.estaEnTexto("1140"));

		assertTrue(bondi60.estaEnTexto("60"));
		assertFalse(bondi60.estaEnTexto("600"));

		assertTrue(bondi160.estaEnTexto("160"));
		assertFalse(bondi160.estaEnTexto("160h"));

		assertTrue(bondi7.estaEnTexto("7"));
		assertFalse(bondi7.estaEnTexto(" 7"));

		assertTrue(bondiTotal.estaEnTexto("Total"));
		assertFalse(bondiTotal.estaEnTexto("Totál"));

		assertTrue(bondi71.estaEnTexto("71"));
		assertFalse(bondi71.estaEnTexto("71 "));

		assertTrue(bondi55.estaEnTexto("55"));
		assertFalse(bondi55.estaEnTexto("5"));

		assertTrue(bondi115.estaEnTexto("115"));
		assertFalse(bondi115.estaEnTexto("11"));

		assertTrue(bondi152.estaEnTexto("152"));
		assertFalse(bondi152.estaEnTexto("15"));

		assertTrue(bondi101.estaEnTexto("101"));
		assertFalse(bondi101.estaEnTexto("1014"));

	}
}