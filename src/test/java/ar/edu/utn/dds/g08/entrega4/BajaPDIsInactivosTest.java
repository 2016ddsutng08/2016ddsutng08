package ar.edu.utn.dds.g08.entrega4;

import java.util.*;

import ar.edu.utn.dds.g08.acciones.*;
import ar.edu.utn.dds.g08.datos.CreadorRepositorioPDI;
import ar.edu.utn.dds.g08.fuentesDatos.RepositorioPDI;
import ar.edu.utn.dds.g08.perfilesUsuarios.PerfilAdministrador;

import org.junit.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class BajaPDIsInactivosTest {
	public static class OrigenBaja implements ActualizacionPDIsActivos {
		BajaPDI pdiCGP = new BajaPDI("Comuna 01", new Date());
		BajaPDI pdiColectivo = new BajaPDI("114", new Date());
		BajaPDI pdiLocalComercial = new BajaPDI("Herencia", new Date());
		BajaPDI pdiSucursal = new BajaPDI("Galicia cheto", new Date());

		public List<BajaPDI> pdisInactivos() {
			return Arrays.asList(pdiCGP, pdiColectivo, pdiLocalComercial,
					pdiSucursal);
		}
	}

	public static class OrigenBajaError implements ActualizacionPDIsActivos {
		public List<BajaPDI> pdisInactivos() {
			throw new RuntimeException();
		}
	}

	RepositorioPDI repositorio = new CreadorRepositorioPDI().buildRepositorio();

	PerfilAdministrador administrador = new PerfilAdministrador("Administrador");

	ActualizacionPDIsActivos interfazGobBAOK = new OrigenBaja();
	ActualizacionPDIsActivos interfazGobBAError = new OrigenBajaError();

	@Test
	public void testBajaOk() {
		Accion accionBaja = new BajaPDIsInactivos(interfazGobBAOK, repositorio);
		NotificadorMail notificador = new NotificadorMail();

		notificador.setUsuario(administrador);

		accionBaja.setNotificador(notificador);

		long cantidad = repositorio.stream().count();

		assertTrue(repositorio.buscarPDI("Comuna 01").size() == 1);
		assertTrue(repositorio.buscarPDI("114").size() == 1);
		assertTrue(repositorio.buscarPDI("Herencia").size() == 1);
		assertTrue(repositorio.buscarPDI("Galicia cheto").size() == 1);

		EstadoProceso estado = administrador.ejecutarProceso(accionBaja);

		while (!estado.estaFinalizado())
			System.out.println("todavía no terminó");

		assertTrue(estado.finalizadoCorrectamente());
		assertTrue(repositorio.buscarPDI("Comuna 01").size() == 0);
		assertTrue(repositorio.buscarPDI("114").size() == 0);
		assertTrue(repositorio.buscarPDI("Herencia").size() == 0);
		assertTrue(repositorio.buscarPDI("Galicia cheto").size() == 0);
		assertTrue(cantidad - 4 == repositorio.stream().count());

	}

	@Test
	public void testBajaErrorMail() {
		Accion accionBaja = new BajaPDIsInactivos(interfazGobBAError,
				repositorio);
		NotificadorMail notificador = new NotificadorMail();
		NotificadorMail espiaNotificador = spy(notificador);

		administrador.setMail("mail@utn.com.ar");
		espiaNotificador.setUsuario(administrador);

		accionBaja.setNotificador(espiaNotificador);

		long cantidad = repositorio.stream().count();

		assertTrue(repositorio.buscarPDI("Comuna 01").size() == 1);
		assertTrue(repositorio.buscarPDI("114").size() == 1);
		assertTrue(repositorio.buscarPDI("Herencia").size() == 1);
		assertTrue(repositorio.buscarPDI("Galicia cheto").size() == 1);

		EstadoProceso estado = administrador.ejecutarProceso(accionBaja);

		while (!estado.estaFinalizado())
			System.out.println("todavía no terminó");

		assertFalse(estado.finalizadoCorrectamente());
		assertTrue(repositorio.buscarPDI("Comuna 01").size() == 1);
		assertTrue(repositorio.buscarPDI("114").size() == 1);
		assertTrue(repositorio.buscarPDI("Herencia").size() == 1);
		assertTrue(repositorio.buscarPDI("Galicia cheto").size() == 1);
		assertTrue(cantidad == repositorio.stream().count());
		verify(espiaNotificador, times(1)).notificar(any(), any());
	}
}
