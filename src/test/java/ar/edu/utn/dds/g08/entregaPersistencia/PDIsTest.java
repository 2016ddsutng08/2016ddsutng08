package ar.edu.utn.dds.g08.entregaPersistencia;

import ar.edu.utn.dds.g08.datos.*;
import ar.edu.utn.dds.g08.dominio.*;

import org.junit.*;

import static org.junit.Assert.*;

public class PDIsTest extends PersistenciaTest {

	@Test
	public void testCGP() {
		CGP cgpTest = new CreadorCGP().buildCGP2();

		withTransaction(() -> {
			this.sesion.persist(cgpTest);
		});

		this.sesion.clear();

		CGP cgpTest2 = this.sesion.find(CGP.class, cgpTest.getId());

		assertEquals(cgpTest2.getComuna().getNumero(), cgpTest.getComuna()
				.getNumero());

		withTransaction(() -> {
			cgpTest2.setTelefono("45458764");
		});

		this.sesion.clear();

		CGP cgpTest3 = this.sesion.find(CGP.class, cgpTest.getId());

		assertEquals("45458764", cgpTest3.getTelefono());

		assertFalse(cgpTest3.estaEnTexto("114"));
		
		assertTrue(cgpTest3.contieneServicio("atención ciudadana"));
		
		withTransaction(() -> {
			this.sesion.delete(cgpTest3);
		});

		CGP cgpTest4 = this.sesion.find(CGP.class, cgpTest.getId());

		assertEquals(null, cgpTest4);
	}

	@Test
	public void testSucursalBanco() {
		SucursalBanco bancoTest = new CreadorSucursalBanco().buildBanco4();

		withTransaction(() -> {
			this.sesion.persist(bancoTest);
		});

		this.sesion.clear();

		SucursalBanco bancoTest2 = this.sesion.find(SucursalBanco.class,
				bancoTest.getId());

		assertEquals(bancoTest2.getBanco(), bancoTest.getBanco());

		withTransaction(() -> {
			bancoTest2.setGerente("matias");
			;
		});

		this.sesion.clear();

		SucursalBanco bancoTest3 = this.sesion.find(SucursalBanco.class,
				bancoTest.getId());

		assertEquals("matias", bancoTest3.getGerente());

		withTransaction(() -> {
			this.sesion.delete(bancoTest3);
		});

		SucursalBanco bancoTest4 = this.sesion.find(SucursalBanco.class,
				bancoTest.getId());

		assertEquals(null, bancoTest4);

	}

	@Test
	public void testLocalComercial() {
		LocalComercial localTest = new CreadorLocalComercial().buildLocal8();

		withTransaction(() -> {
			this.sesion.persist(localTest);
		});

		this.sesion.clear();

		LocalComercial localTest2 = this.sesion.find(LocalComercial.class,
				localTest.getId());

		assertEquals(localTest2.getNombreFantasia(),
				localTest.getNombreFantasia());

		withTransaction(() -> {
			localTest2.addPalabraClave("milanesa");
		});

		this.sesion.clear();

		LocalComercial localTest3 = this.sesion.find(LocalComercial.class,
				localTest.getId());

		assertTrue(localTest3.getPalabrasClaves().contains("milanesa"));

		withTransaction(() -> {
			this.sesion.delete(localTest3);
		});

		LocalComercial localTest4 = this.sesion.find(LocalComercial.class,
				localTest.getId());

		assertEquals(null, localTest4);

	}

	@Test
	public void testColectivo() {
		// tiene que actualizar también las paradas

		Colectivo colectivoTest = new CreadorColectivo().buildColectivo4();

		withTransaction(() -> {
			colectivoTest.getParadas().forEach(p -> this.sesion.persist(p));
			this.sesion.persist(colectivoTest);
		});

		this.sesion.clear();

		Colectivo colectivoTest2 = this.sesion.find(Colectivo.class,
				colectivoTest.getId());

		assertEquals(colectivoTest2.getLinea(), colectivoTest.getLinea());

		Parada parada1 = new CreadorParada().buildParada1();
		Parada parada4 = new CreadorParada().buildParada4();

		withTransaction(() -> {
			this.sesion.persist(parada1);
			this.sesion.persist(parada4);
			colectivoTest2.addParada(parada1);
			colectivoTest2.addParada(parada4);
		});

		this.sesion.clear();

		Colectivo colectivoTest3 = this.sesion.find(Colectivo.class,
				colectivoTest.getId());

		assertTrue(colectivoTest3.getParadas().contains(parada1));
		assertTrue(colectivoTest3.getParadas().contains(parada4));

		withTransaction(() -> {
			colectivoTest3.getParadas().forEach(p -> this.sesion.delete(p));
			this.sesion.delete(colectivoTest3);
		});

		Colectivo colectivoTest4 = this.sesion.find(Colectivo.class,
				colectivoTest.getId());

		assertEquals(null, colectivoTest4);

	}

}
