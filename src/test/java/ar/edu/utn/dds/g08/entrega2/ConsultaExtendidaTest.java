package ar.edu.utn.dds.g08.entrega2;

import java.util.Arrays;

import org.junit.Test;

import static org.junit.Assert.*;
import ar.edu.utn.dds.g08.busquedas.*;
import ar.edu.utn.dds.g08.datos.CreadorRepositorioPDI;
import ar.edu.utn.dds.g08.fuentesDatos.RepositorioPDI;

public class ConsultaExtendidaTest {
	RepositorioPDI repo = new CreadorRepositorioPDI().buildRepositorio();

	BuscadorPDIs repoSist = new BuscadorRepositorio(repo);

	BuscadorPDIs repoCGP = new BuscadorCGPs(new ConsultaCGPsTest.OrigenCGP());

	BuscadorPDIs repoBanco = new BuscadorBancos(new ConsultaBancosTest.OrigenBanco());

	BuscadorPDIs extendido = new BuscadorExtendido(Arrays.asList(repoSist,
			repoCGP, repoBanco));

	@Test
	public void consultaTest() {
		assertTrue(extendido.buscarPDI("Santander", "depósitos").size() == 4);
		assertTrue(extendido.buscarPDI("60").size() == 1);
		assertTrue(extendido.buscarPDI("Palermo").size() == 1);
		assertTrue(extendido.buscarPDI("500").size() == 3);
		assertTrue(extendido.buscarPDI("Bondiola").size() == 3);
	}
}
