package ar.edu.utn.dds.g08.entrega3;

import ar.edu.utn.dds.g08.valueClasses.*;

import org.joda.time.*;
import org.junit.*;
import static org.junit.Assert.*;
import java.util.*;

public class GeneradorReportesTest {
	GeneradorReportes reportes;

	@Test
	public void testBusquedasPorFecha() {
		Map<LocalDate, Long> resultados;
		RegistroLogBusqueda log;
		LocalDate fecha;

		reportes = new GeneradorReportes();

		log = new RegistroLogBusqueda("Frase 1", 2, 150, new DateTime(2016, 1,
				3, 10, 0), "Usuario 1");
		reportes.addBusqueda(log);

		log = new RegistroLogBusqueda("Frase 2", 3, 150, new DateTime(2016, 1,
				3, 10, 0), "Usuario 2");
		reportes.addBusqueda(log);

		log = new RegistroLogBusqueda("Frase 3", 4, 150, new DateTime(2016, 1,
				4, 10, 0), "Usuario 1");
		reportes.addBusqueda(log);

		resultados = reportes.busquedasPorFecha();

		assertTrue(resultados.size() == 2);

		fecha = new LocalDate(2016, 1, 3);
		assertTrue(resultados.containsKey(fecha));
		assertTrue(resultados.get(fecha) == 2);

		fecha = new LocalDate(2016, 1, 4);
		assertTrue(resultados.containsKey(fecha));
		assertTrue(resultados.get(fecha) == 1);
	}

	@Test
	public void testBusquedasPorFechaPorTerminal() {
		Map<LocalDate, Long> resultados;
		RegistroLogBusqueda log;
		LocalDate fecha;

		reportes = new GeneradorReportes();

		log = new RegistroLogBusqueda("Frase 1", 2, 150, new DateTime(2016, 1,
				3, 10, 0), "Usuario 1");
		reportes.addBusqueda(log);

		log = new RegistroLogBusqueda("Frase 2", 3, 150, new DateTime(2016, 1,
				3, 10, 0), "Usuario 2");
		reportes.addBusqueda(log);

		log = new RegistroLogBusqueda("Frase 3", 4, 150, new DateTime(2016, 1,
				4, 10, 0), "Usuario 1");
		reportes.addBusqueda(log);

		resultados = reportes.busquedasPorFechaPorTerminal("Usuario 1");

		assertTrue(resultados.size() == 2);

		fecha = new LocalDate(2016, 1, 3);
		assertTrue(resultados.containsKey(fecha));
		assertTrue(resultados.get(fecha) == 1);

		fecha = new LocalDate(2016, 1, 4);
		assertTrue(resultados.containsKey(fecha));
		assertTrue(resultados.get(fecha) == 1);

		resultados = reportes.busquedasPorFechaPorTerminal("Usuario 15");

		assertTrue(resultados.size() == 0);
	}

	@Test
	public void testResultadosPorBusqueda() {
		Map<String, Integer> resultados;
		RegistroLogBusqueda log;

		reportes = new GeneradorReportes();

		log = new RegistroLogBusqueda("Frase 1", 2, 150, new DateTime(2016, 1,
				3, 10, 0), "Usuario 1");
		reportes.addBusqueda(log);

		log = new RegistroLogBusqueda("Frase 2", 2, 150, new DateTime(2016, 1,
				3, 10, 0), "Usuario 1");
		reportes.addBusqueda(log);

		log = new RegistroLogBusqueda("Frase 2", 2, 150, new DateTime(2016, 1,
				3, 10, 0), "Usuario 2");
		reportes.addBusqueda(log);

		resultados = reportes.resultadosPorBusqueda();

		assertTrue(resultados.size() == 2);

		assertTrue(resultados.containsKey("Usuario 1"));
		assertTrue(resultados.get("Usuario 1") == 4);

		assertTrue(resultados.containsKey("Usuario 2"));
		assertTrue(resultados.get("Usuario 2") == 2);
	}

	@Test
	public void testResultadosPorBusquedaPorTerminal() {
		RegistroLogBusqueda log;
		List<Integer> resultados;

		reportes = new GeneradorReportes();

		log = new RegistroLogBusqueda("Frase 1", 2, 150, new DateTime(2016, 1,
				3, 10, 0), "Usuario 1");
		reportes.addBusqueda(log);

		log = new RegistroLogBusqueda("Frase 2", 3, 150, new DateTime(2016, 1,
				3, 10, 0), "Usuario 1");
		reportes.addBusqueda(log);

		log = new RegistroLogBusqueda("Frase 2", 2, 150, new DateTime(2016, 1,
				3, 10, 0), "Usuario 2");
		reportes.addBusqueda(log);

		resultados = reportes.resultadosPorBusquedaPorTerminal("Usuario 1");

		assertTrue(resultados.contains(2));
		assertTrue(resultados.contains(3));
		assertTrue(resultados.size() == 2);

		resultados = reportes.resultadosPorBusquedaPorTerminal("Usuario 2");

		assertTrue(resultados.contains(2));
		assertTrue(resultados.size() == 1);
	}
}
