package ar.edu.utn.dds.g08.entrega2;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.*;

import org.junit.Test;

import ar.edu.utn.dds.g08.busquedas.BuscadorBancos;
import ar.edu.utn.dds.g08.busquedas.BuscadorCache;
import ar.edu.utn.dds.g08.fuentesDatos.*;

import org.json.*;

public class ConsultaBancosTest {
	public static class OrigenBanco implements ConsultaBancos {
		public JSONArray repositorio;

		public OrigenBanco() {
			String strBancos = "["
					+ "{\"banco\": \"Francés\", \"x\": \"-34.413290\", \"y\": \"-58.636901\", \"sucursal\": \"UTN\", \"gerente\": \"Lusi Eugenio\", \"servicios\": [\"depósitos\",\"extracciones\",\"transferencias\"]},"
					+ "{\"banco\": \"Francés\", \"x\": \"-34.413570\", \"y\": \"-57.636901\", \"sucursal\": \"Flores\", \"gerente\": \"Florero\", \"servicios\": [\"depósitos\",\"extracciones\",\"transferencias\",\"créditos\",\"seguros\"]},"
					+ "{\"banco\": \"ICBC\", \"x\": \"-35.413290\", \"y\": \"-56.636901\", \"sucursal\": \"Casa\", \"gerente\": \"Yo\", \"servicios\": [\"extracciones\"]},"
					+ "{\"banco\": \"ICBC\", \"x\": \"-33.413290\", \"y\": \"-58.637901\", \"sucursal\": \"Olivos\", \"gerente\": \"Marian\", \"servicios\": [\"depósitos\",\"extracciones\"]},"
					+ "{\"banco\": \"ICBC\", \"x\": \"-34.417290\", \"y\": \"-58.638101\", \"sucursal\": \"Retiro\", \"gerente\": \"Nose\", \"servicios\": [\"depósitos\",\"extracciones\",\"transferencias\",\"créditos\",\"seguros\"]},"
					+ "{\"banco\": \"Santander\", \"x\": \"-34.413890\", \"y\": \"-58.636976\", \"sucursal\": \"Santander1\", \"gerente\": \"Gallego1\", \"servicios\": [\"depósitos\",\"extracciones\",\"transferencias\"]},"
					+ "{\"banco\": \"Santander\", \"x\": \"-34.413490\", \"y\": \"-58.645901\", \"sucursal\": \"Santander2\", \"gerente\": \"Gallego2\", \"servicios\": [\"depósitos\",\"extracciones\",\"transferencias\",\"créditos\",\"seguros\"]},"
					+ "{\"banco\": \"Santander\", \"x\": \"-34.413530\", \"y\": \"-58.365901\", \"sucursal\": \"Santander3\", \"gerente\": \"Gallego3\", \"servicios\": [\"cheques\"]},"
					+ "{\"banco\": \"Santander\", \"x\": \"-34.421290\", \"y\": \"-58.629801\", \"sucursal\": \"Santander4\", \"gerente\": \"Gallego4\", \"servicios\": [\"depósitos\",\"extracciones\",\"transferencias\",\"créditos\",\"seguros\"]},"
					+ "{\"banco\": \"Santander\", \"x\": \"-34.567290\", \"y\": \"-58.631531\", \"sucursal\": \"Santander5\", \"gerente\": \"Gallego5\", \"servicios\": [\"depósitos\",\"extracciones\",\"transferencias\"]}"
					+ "]";

			repositorio = new JSONArray(strBancos);
		}

		public JSONArray buscarBancos(String nombre, String servicio) {
			Iterator<Object> it = repositorio.iterator();
			Iterator<Object> it2;

			JSONObject json;

			JSONArray filter = new JSONArray();

			while (it.hasNext()) {
				json = (JSONObject) it.next();
				if (json.getString("banco").contains(nombre)) {
					it2 = json.getJSONArray("servicios").iterator();

					while (it2.hasNext()) {
						if (it2.next().toString().contains(servicio)) {
							filter.put(json);
							break;
						}
					}
				}
			}

			return filter;
		}
	}

	@Test
	public void testConsulta() {
		OrigenBanco origen = new OrigenBanco();

		assertTrue(origen.buscarBancos("Santander", "depósitos").length() == 4);
		assertTrue(origen.buscarBancos("San", "depósitos").length() == 4);
		assertTrue(origen.buscarBancos("San", "depósitos").length() == 4);
		assertTrue(origen.buscarBancos("Itaú", "depósitos").length() == 0);
		assertTrue(origen.buscarBancos("ICBC", "seguros").length() == 1);
	}

	@Test
	public void testCache() {
		OrigenBanco origen = mock(OrigenBanco.class);
		OrigenBanco origenReal = new OrigenBanco();
		BuscadorCache bancoCache = new BuscadorCache(new BuscadorBancos(origen));

		when(origen.buscarBancos("ICBC", "seguros")).thenReturn(
				origenReal.buscarBancos("ICBC", "seguros"));

		verify(origen, times(0)).buscarBancos("ICBC", "seguros");

		assertTrue(bancoCache.buscarPDI("ICBC", "seguros").size() == 1);

		verify(origen, times(1)).buscarBancos("ICBC", "seguros");

		assertTrue(bancoCache.buscarPDI("ICBC", "seguros").size() == 1);

		verify(origen, times(1)).buscarBancos("ICBC", "seguros");
	}
}
