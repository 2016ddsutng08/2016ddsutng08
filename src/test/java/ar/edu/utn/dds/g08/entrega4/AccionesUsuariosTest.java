package ar.edu.utn.dds.g08.entrega4;

import ar.edu.utn.dds.g08.acciones.*;
import ar.edu.utn.dds.g08.datos.*;
import ar.edu.utn.dds.g08.fuentesDatos.RepositorioPDI;
import ar.edu.utn.dds.g08.perfilesUsuarios.*;

import org.junit.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.*;

public class AccionesUsuariosTest {
	RepositorioPDI repositorio = new CreadorRepositorioPDI().buildRepositorio();

	PerfilAdministrador administrador = new PerfilAdministrador("Administrador");

	LogueadorBusqueda logueador = new LogueadorBusqueda();
	ObservadorBusqueda notDemora = new NotificadorDemora(administrador, 2000);

	@Test
	public void testActualizacionOK() {
		AccionesUsuarios accionActualizar = new AccionesUsuarios();
		Reintentar reintentos = new Reintentar();

		reintentos.setReintentos(1);
		reintentos.setAccionAsociada(accionActualizar);

		accionActualizar.setNotificador(reintentos);

		CreadorPerfilTerminal builderTerminal = new CreadorPerfilTerminal();
		builderTerminal.setLogueador(logueador);

		PerfilTerminal terminalVacio = builderTerminal.buildTerminalVacio();
		PerfilTerminal terminalLog = builderTerminal.buildTerminalLogueador();

		accionActualizar.addAccion(terminalVacio,
				Arrays.asList(logueador, notDemora));
		accionActualizar.addAccion(terminalLog, Arrays.asList(notDemora));

		assertTrue(terminalVacio.getObservadores().size() == 0);
		assertTrue(terminalLog.getObservadores().size() == 1);
		assertTrue(terminalLog.getObservadores().contains(logueador));

		EstadoProceso estado = administrador.ejecutarProceso(accionActualizar);

		while (!estado.estaFinalizado())
			System.out.println("todavía no terminó");

		assertTrue(estado.finalizadoCorrectamente());
		assertTrue(terminalVacio.getObservadores().size() == 2);
		assertTrue(terminalLog.getObservadores().size() == 1);
		assertTrue(terminalVacio.getObservadores().contains(logueador));
		assertTrue(terminalVacio.getObservadores().contains(notDemora));
		assertFalse(terminalLog.getObservadores().contains(logueador));
		assertTrue(terminalLog.getObservadores().contains(notDemora));
	}

	@Test
	public void testActualizacionDeshacer() {
		AccionesUsuarios accionActualizar = new AccionesUsuarios();
		Reintentar reintentos = new Reintentar();

		reintentos.setReintentos(2);
		reintentos.setAccionAsociada(accionActualizar);

		accionActualizar.setNotificador(reintentos);

		CreadorPerfilTerminal builderTerminal = new CreadorPerfilTerminal();
		builderTerminal.setLogueador(logueador);

		PerfilTerminal terminalVacio = builderTerminal.buildTerminalVacio();
		PerfilTerminal terminalLog = builderTerminal.buildTerminalLogueador();

		accionActualizar.addAccion(terminalVacio,
				Arrays.asList(logueador, notDemora));
		accionActualizar.addAccion(terminalLog, Arrays.asList(notDemora));

		assertTrue(terminalVacio.getObservadores().size() == 0);
		assertTrue(terminalLog.getObservadores().size() == 1);
		assertTrue(terminalLog.getObservadores().contains(logueador));

		EstadoProceso estado = administrador.ejecutarProceso(accionActualizar);

		while (!estado.estaFinalizado())
			System.out.println("todavía no terminó");

		assertTrue(estado.finalizadoCorrectamente());
		assertTrue(terminalVacio.getObservadores().size() == 2);
		assertTrue(terminalLog.getObservadores().size() == 1);
		assertTrue(terminalVacio.getObservadores().contains(logueador));
		assertTrue(terminalVacio.getObservadores().contains(notDemora));
		assertFalse(terminalLog.getObservadores().contains(logueador));
		assertTrue(terminalLog.getObservadores().contains(notDemora));

		estado = administrador.deshacerProceso(accionActualizar);

		while (!estado.estaFinalizado())
			System.out.println("todavía no terminó");

		assertTrue(estado.finalizadoCorrectamente());
		assertFalse(terminalVacio.getObservadores().size() == 2);
		assertTrue(terminalLog.getObservadores().size() == 1);
		assertFalse(terminalVacio.getObservadores().contains(logueador));
		assertFalse(terminalVacio.getObservadores().contains(notDemora));
		assertTrue(terminalLog.getObservadores().contains(logueador));
		assertFalse(terminalLog.getObservadores().contains(notDemora));

		assertTrue(terminalVacio.getObservadores().size() == 0);
		assertTrue(terminalLog.getObservadores().size() == 1);
		assertTrue(terminalLog.getObservadores().contains(logueador));

	}

	@Test
	public void testActualizacionErrorReintentar() {
		AccionesUsuarios accionActualizar = new AccionesUsuarios();
		AccionesUsuarios espiaAccion = spy(accionActualizar);

		Reintentar reintentos = new Reintentar();

		reintentos.setReintentos(199);
		reintentos.setAccionAsociada(espiaAccion);

		espiaAccion.setNotificador(reintentos);

		PerfilTerminal terminal = mock(PerfilTerminal.class);

		doThrow(new RuntimeException()).when(terminal).setObservadores(any());

		espiaAccion.addAccion(terminal, Arrays.asList(logueador, notDemora));

		EstadoProceso estado = administrador.ejecutarProceso(espiaAccion);

		while (!estado.estaFinalizado())
			System.out.println("todavía no terminó");

		assertFalse(estado.finalizadoCorrectamente());
		assertTrue(terminal.getObservadores().size() == 0);
		verify(espiaAccion, times(200)).ejecutar(any());

	}
}
