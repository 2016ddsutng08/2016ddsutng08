package ar.edu.utn.dds.g08.entrega4;

import ar.edu.utn.dds.g08.acciones.*;
import ar.edu.utn.dds.g08.datos.CreadorRepositorioPDI;
import ar.edu.utn.dds.g08.fuentesDatos.RepositorioPDI;
import ar.edu.utn.dds.g08.perfilesUsuarios.PerfilAdministrador;

import org.junit.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ActualizacionLocalesComercialesTest {

	RepositorioPDI repositorio = new CreadorRepositorioPDI().buildRepositorio();

	PerfilAdministrador administrador = new PerfilAdministrador("Administrador");

	@Test
	public void testActualizacion() {
		Accion accionActualizar = new ActualizacionLocalesComerciales(
				"/home/dds/git/2016ddsutng08/src/test/resources/actualizacionLocalesComercialesTest.txt",
				repositorio);
		accionActualizar.setNotificador(new NotificadorVacio());

		assertTrue(repositorio.buscarPDI("Camisa").size() == 1);
		assertTrue(repositorio.buscarPDI("Chupin").size() == 0);

		EstadoProceso estado = administrador.ejecutarProceso(accionActualizar);

		while (!estado.estaFinalizado())
			System.out.println("todavía no terminó");

		assertTrue(estado.finalizadoCorrectamente());
		assertTrue(repositorio.buscarPDI("Camisa").size() == 0);
		assertTrue(repositorio.buscarPDI("Chupin").size() == 1);
	}

	@Test
	public void testCreacion() {
		Accion accionActualizar = new ActualizacionLocalesComerciales(
				"/home/dds/git/2016ddsutng08/src/test/resources/actualizacionLocalesComercialesTest.txt",
				repositorio);
		accionActualizar.setNotificador(new NotificadorVacio());

		long cantidad = repositorio.stream().count();

		assertTrue(repositorio.buscarPDI("Levis").size() == 0);
		assertTrue(repositorio.buscarPDI("Chalina").size() == 0);

		EstadoProceso estado = administrador.ejecutarProceso(accionActualizar);

		while (!estado.estaFinalizado())
			System.out.println("todavía no terminó");

		assertTrue(estado.finalizadoCorrectamente());
		assertTrue(repositorio.buscarPDI("Levis").size() == 1);
		assertTrue(repositorio.buscarPDI("Chalina").size() == 1);
		assertTrue(cantidad + 1 == repositorio.stream().count());

	}

	@Test
	public void testErrorNoHacerNada() {
		Accion accionActualizar = new ActualizacionLocalesComerciales(
				"/home/dds/git/2016ddsutng08/src/test/resources/actualizacionLocalesComercialesTestError.txt",
				repositorio);
		Notificador notificador = new NotificadorVacio();
		Notificador espiaNotificador = spy(notificador);

		accionActualizar.setNotificador(espiaNotificador);

		long cantidad = repositorio.stream().count();

		assertTrue(repositorio.buscarPDI("Levis").size() == 0);
		assertTrue(repositorio.buscarPDI("Chalina").size() == 0);
		assertTrue(repositorio.buscarPDI("Camisa").size() == 1);
		assertTrue(repositorio.buscarPDI("Chupin").size() == 0);

		EstadoProceso estado = administrador.ejecutarProceso(accionActualizar);

		while (!estado.estaFinalizado())
			System.out.println("todavía no terminó");

		assertFalse(estado.finalizadoCorrectamente());
		assertTrue(repositorio.buscarPDI("Levis").size() == 0);
		assertTrue(repositorio.buscarPDI("Chalina").size() == 0);
		assertTrue(repositorio.buscarPDI("Camisa").size() == 1);
		assertTrue(repositorio.buscarPDI("Chupin").size() == 0);
		assertTrue(cantidad == repositorio.stream().count());
		verify(espiaNotificador, times(1)).notificar(any(), any());

	}

}
