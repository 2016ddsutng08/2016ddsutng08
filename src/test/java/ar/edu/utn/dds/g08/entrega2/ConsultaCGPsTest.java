package ar.edu.utn.dds.g08.entrega2;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import static org.junit.Assert.*;
import ar.edu.utn.dds.g08.busquedas.BuscadorCGPs;
import ar.edu.utn.dds.g08.busquedas.BuscadorCache;
import ar.edu.utn.dds.g08.fuentesDatos.*;

import static org.mockito.Mockito.*;

public class ConsultaCGPsTest {
	public static class OrigenCGP implements ConsultaCGPs {
		public List<CentroDTO> repositorio;

		public OrigenCGP() {
			RangoServicioDTO lunes = new RangoServicioDTO(1, 9, 18, 0, 0);
			RangoServicioDTO martes = new RangoServicioDTO(2, 9, 18, 0, 0);
			RangoServicioDTO miercoles = new RangoServicioDTO(3, 9, 18, 0, 0);
			RangoServicioDTO jueves = new RangoServicioDTO(4, 9, 18, 0, 0);
			RangoServicioDTO viernes = new RangoServicioDTO(5, 9, 18, 0, 0);
			RangoServicioDTO sabado = new RangoServicioDTO(6, 10, 14, 0, 0);

			ServicioDTO atCiudadana = new ServicioDTO("Atención Ciudadana",
					Arrays.asList(lunes, martes, miercoles, jueves, viernes,
							sabado));
			ServicioDTO quejas = new ServicioDTO("Quejas", Arrays.asList(lunes,
					miercoles, viernes));
			ServicioDTO eventos = new ServicioDTO("Eventos",
					Arrays.asList(sabado));

			CentroDTO cgp1 = new CentroDTO(1, "Palermo", "Fede",
					"Serrano 1500", "011 15555", Arrays.asList(eventos,
							atCiudadana));
			CentroDTO cgp2 = new CentroDTO(2, "Nuñez", "Carla",
					"Figueroa Alcorta 2500", "011 15645", Arrays.asList(
							eventos, quejas));
			CentroDTO cgp3 = new CentroDTO(3, "Devoto", "Gaby",
					"Nueva York 500", "011 159823", Arrays.asList(eventos,
							atCiudadana, quejas));
			CentroDTO cgp4 = new CentroDTO(4, "Retiro", "Yo", "Alem 1110",
					"011 1501231", Arrays.asList(eventos, atCiudadana, quejas));
			CentroDTO cgp5 = new CentroDTO(5, "Saavedra", "Marian",
					"25 de Mayo 1810", "011 151723", Arrays.asList(atCiudadana,
							quejas));

			repositorio = Arrays.asList(cgp1, cgp2, cgp3, cgp4, cgp5);
		}

		public List<CentroDTO> buscarCGPs(String textoConsulta) {
			return repositorio
					.stream()
					.filter(c -> c.getZonas().contains(textoConsulta)
							|| c.getDomicilio().contains(textoConsulta))
					.collect(Collectors.toList());
		}
	}

	@Test
	public void testConsulta() {
		OrigenCGP origen = new OrigenCGP();

		assertFalse(origen.buscarCGPs("aler").isEmpty());
		assertTrue(origen.buscarCGPs("Palerme").isEmpty());

		assertTrue(origen.buscarCGPs("Malnatti").isEmpty());
		assertFalse(origen.buscarCGPs("Figueroa").isEmpty());

		assertTrue(origen.buscarCGPs("500").size() == 3);
	}

	@Test
	public void testCache() {
		OrigenCGP origen = mock(OrigenCGP.class);
		OrigenCGP origenReal = new OrigenCGP();
		BuscadorCache cgpCache = new BuscadorCache(new BuscadorCGPs(origen));
		
		when(origen.buscarCGPs("500")).thenReturn(origenReal.buscarCGPs("500"));
		
		verify(origen,times(0)).buscarCGPs("500");
		
		assertTrue(cgpCache.buscarPDI("500").size() == 3);
		
		verify(origen,times(1)).buscarCGPs("500");
		
		assertTrue(cgpCache.buscarPDI("500").size() == 3);
		
		verify(origen,times(1)).buscarCGPs("500");
	}
}
