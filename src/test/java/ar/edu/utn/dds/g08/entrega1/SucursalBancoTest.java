package ar.edu.utn.dds.g08.entrega1;

import org.joda.time.DateTime;
import org.joda.time.Instant;
import org.junit.Test;

import static org.junit.Assert.*;

import ar.edu.utn.dds.g08.datos.*;
import ar.edu.utn.dds.g08.dominio.*;
import ar.edu.utn.dds.g08.valueClasses.*;

public class SucursalBancoTest {
	SucursalBanco sucBoca = new CreadorSucursalBanco().buildBanco1();
	SucursalBanco sucRiber = new CreadorSucursalBanco().buildBanco2();
	SucursalBanco sucOlivos = new CreadorSucursalBanco().buildBanco3();
	SucursalBanco sucAvellaneda = new CreadorSucursalBanco().buildBanco4();
	SucursalBanco sucLugano = new CreadorSucursalBanco().buildBanco5();
	SucursalBanco sucPalermo = new CreadorSucursalBanco().buildBanco6();
	SucursalBanco sucPacheco = new CreadorSucursalBanco().buildBanco7();
	SucursalBanco sucSanMiguel = new CreadorSucursalBanco().buildBanco8();
	SucursalBanco sucAtlas = new CreadorSucursalBanco().buildBanco9();
	SucursalBanco sucJCP = new CreadorSucursalBanco().buildBanco10();

	@Test
	public void testCercania() {
		PointAdaptado puntoCercano = new PointAdaptado(-34.633313, -58.367634);

		assertTrue(sucBoca.esCercano(puntoCercano));

		puntoCercano = new PointAdaptado(-34.544355, -58.450635);
		assertTrue(sucRiber.esCercano(puntoCercano));

		puntoCercano = new PointAdaptado(-34.507449, -58.508000);
		assertTrue(sucOlivos.esCercano(puntoCercano));

		puntoCercano = new PointAdaptado(-34.667153, -58.371609);
		assertTrue(sucAvellaneda.esCercano(puntoCercano));

		puntoCercano = new PointAdaptado(-34.657573, -58.471702);
		assertTrue(sucLugano.esCercano(puntoCercano));

		PointAdaptado puntoLejano = new PointAdaptado(-34.603703, -58.381773);
		assertFalse(sucPalermo.esCercano(puntoLejano));
		assertFalse(sucPacheco.esCercano(puntoLejano));
		assertFalse(sucSanMiguel.esCercano(puntoLejano));
		assertFalse(sucAtlas.esCercano(puntoLejano));
		assertFalse(sucJCP.esCercano(puntoLejano));
	}

	@Test
	public void testDisponibilidad() {
		DateTime hora = new DateTime(2016, 07, 15, 14, 59);
		Instant horaDisponible = hora.toInstant();

		assertTrue(sucBoca.estaDisponible(horaDisponible));
		assertTrue(sucRiber.estaDisponible(horaDisponible));
		assertTrue(sucOlivos.estaDisponible(horaDisponible));
		assertTrue(sucAvellaneda.estaDisponible(horaDisponible));
		assertTrue(sucLugano.estaDisponible(horaDisponible));
		assertTrue(sucPalermo.estaDisponible(horaDisponible));
		assertTrue(sucPacheco.estaDisponible(horaDisponible));
		assertTrue(sucSanMiguel.estaDisponible(horaDisponible));
		assertTrue(sucAtlas.estaDisponible(horaDisponible));
		assertTrue(sucJCP.estaDisponible(horaDisponible));

		hora = new DateTime(2016, 07, 15, 15, 01);
		Instant horaNODisponible = hora.toInstant();

		assertFalse(sucBoca.estaDisponible(horaNODisponible));
		assertFalse(sucRiber.estaDisponible(horaNODisponible));
		assertFalse(sucOlivos.estaDisponible(horaNODisponible));
		assertFalse(sucAvellaneda.estaDisponible(horaNODisponible));
		assertFalse(sucLugano.estaDisponible(horaNODisponible));
		assertFalse(sucPalermo.estaDisponible(horaNODisponible));
		assertFalse(sucPacheco.estaDisponible(horaNODisponible));
		assertFalse(sucSanMiguel.estaDisponible(horaNODisponible));
		assertFalse(sucAtlas.estaDisponible(horaNODisponible));
		assertFalse(sucJCP.estaDisponible(horaNODisponible));
	}

	@Test
	public void testBusqueda() {
		assertTrue(sucBoca.estaEnTexto("Galicia"));
		assertTrue(sucRiber.estaEnTexto("Galicia"));
		assertTrue(sucOlivos.estaEnTexto("Galicia"));
		assertTrue(sucAvellaneda.estaEnTexto("Francés"));
		assertTrue(sucLugano.estaEnTexto("Francés"));
		assertTrue(sucPalermo.estaEnTexto("Francés"));
		assertTrue(sucAtlas.estaEnTexto("Francés"));
		assertTrue(sucJCP.estaEnTexto("Francés"));
		assertTrue(sucPacheco.estaEnTexto("ICBC"));
		assertTrue(sucSanMiguel.estaEnTexto("IC"));

		assertFalse(sucBoca.estaEnTexto("Francés"));
		assertFalse(sucRiber.estaEnTexto("Francés"));
		assertFalse(sucOlivos.estaEnTexto("Francés"));
		assertFalse(sucAvellaneda.estaEnTexto("Galicia"));
		assertFalse(sucLugano.estaEnTexto("Galicia"));
		assertFalse(sucPalermo.estaEnTexto("Galicia"));
		assertFalse(sucAtlas.estaEnTexto("Galicia"));
		assertFalse(sucJCP.estaEnTexto("Galicia"));
		assertFalse(sucPacheco.estaEnTexto("H"));
		assertFalse(sucSanMiguel.estaEnTexto("H"));
	}

}
