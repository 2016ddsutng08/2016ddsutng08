package ar.edu.utn.dds.g08.entrega1;

import org.junit.Test;

import static org.junit.Assert.*;

import org.joda.time.*;

import ar.edu.utn.dds.g08.datos.*;
import ar.edu.utn.dds.g08.dominio.CGP;
import ar.edu.utn.dds.g08.valueClasses.*;

public class CGPTest {
	public CGP dep01Com01 = new CreadorCGP().buildCGP1();
	public CGP dep02Com01 = new CreadorCGP().buildCGP2();
	public CGP dep03Com01 = new CreadorCGP().buildCGP3();
	public CGP dep01Com02 = new CreadorCGP().buildCGP4();
	public CGP dep02Com02 = new CreadorCGP().buildCGP5();
	public CGP dep03Com02 = new CreadorCGP().buildCGP6();
	public CGP dep04Com02 = new CreadorCGP().buildCGP7();
	public CGP dep01Com03 = new CreadorCGP().buildCGP8();
	public CGP dep02Com03 = new CreadorCGP().buildCGP9();
	public CGP dep03Com03 = new CreadorCGP().buildCGP10();

	@Test
	public void testCercania() {
		PointAdaptado puntoCercano = new PointAdaptado(-34.658953, -58.469015);

		assertTrue(dep01Com01.esCercano(puntoCercano));
		assertTrue(dep02Com01.esCercano(puntoCercano));
		assertTrue(dep03Com01.esCercano(puntoCercano));

		puntoCercano = new PointAdaptado(-34.659034, -58.466672);
		assertTrue(dep01Com02.esCercano(puntoCercano));
		assertTrue(dep02Com02.esCercano(puntoCercano));
		assertTrue(dep03Com02.esCercano(puntoCercano));
		assertTrue(dep04Com02.esCercano(puntoCercano));

		puntoCercano = new PointAdaptado(-34.662175, -58.467404);
		assertTrue(dep01Com03.esCercano(puntoCercano));
		assertTrue(dep02Com03.esCercano(puntoCercano));
		assertTrue(dep03Com03.esCercano(puntoCercano));

		PointAdaptado puntoLejano = new PointAdaptado(-34.636926, -58.478617);
		assertFalse(dep01Com01.esCercano(puntoLejano));
		assertFalse(dep02Com01.esCercano(puntoLejano));
		assertFalse(dep03Com01.esCercano(puntoLejano));

		assertFalse(dep01Com02.esCercano(puntoLejano));
		assertFalse(dep02Com02.esCercano(puntoLejano));
		assertFalse(dep03Com02.esCercano(puntoLejano));
		assertFalse(dep04Com02.esCercano(puntoLejano));

		assertFalse(dep01Com03.esCercano(puntoLejano));
		assertFalse(dep02Com03.esCercano(puntoLejano));
		assertFalse(dep03Com03.esCercano(puntoLejano));
	}

	@Test
	public void testDisponibilidad() {
		DateTime hora = new DateTime(2016, 07, 15, 15, 00);
		Instant horaDisponible = hora.toInstant();
		assertFalse(dep01Com01.estaDisponible(horaDisponible, "Kiosko"));
		assertTrue(dep01Com01.estaDisponible(horaDisponible));
		assertTrue(dep01Com01.estaDisponible(horaDisponible,
				"atención ciudadana"));
		assertTrue(dep02Com01.estaDisponible(horaDisponible));
		assertTrue(dep03Com01.estaDisponible(horaDisponible));

		assertTrue(dep01Com02.estaDisponible(horaDisponible));
		assertTrue(dep02Com02.estaDisponible(horaDisponible));
		assertTrue(dep03Com02.estaDisponible(horaDisponible));
		assertTrue(dep04Com02.estaDisponible(horaDisponible));

		assertTrue(dep01Com03.estaDisponible(horaDisponible));
		assertTrue(dep02Com03.estaDisponible(horaDisponible));
		assertTrue(dep03Com03.estaDisponible(horaDisponible));

		hora = new DateTime(2016, 07, 17, 15, 00);
		Instant horaNODisponible = hora.toInstant();
		assertTrue(dep01Com01.estaDisponible(horaNODisponible));
		assertFalse(dep01Com01.estaDisponible(horaNODisponible,
				"atención ciudadana"));
		assertTrue(dep01Com01.estaDisponible(horaNODisponible, "Kiosko"));
		assertFalse(dep02Com01.estaDisponible(horaNODisponible));
		assertFalse(dep03Com01.estaDisponible(horaNODisponible));

		assertFalse(dep01Com02.estaDisponible(horaNODisponible));
		assertFalse(dep02Com02.estaDisponible(horaNODisponible));
		assertFalse(dep03Com02.estaDisponible(horaNODisponible));
		assertFalse(dep04Com02.estaDisponible(horaNODisponible));

		assertFalse(dep01Com03.estaDisponible(horaNODisponible));
		assertFalse(dep02Com03.estaDisponible(horaNODisponible));
		assertFalse(dep03Com03.estaDisponible(horaNODisponible));
	}

	@Test
	public void testBusqueda() {
		assertTrue(dep01Com01.estaEnTexto("Comuna 01"));
		assertTrue(dep01Com01.estaEnTexto("atención"));
		assertTrue(dep02Com01.estaEnTexto("Comuna 01"));
		assertTrue(dep02Com01.estaEnTexto("atención"));
		assertTrue(dep03Com01.estaEnTexto("Comuna 01"));
		assertTrue(dep03Com01.estaEnTexto("atención"));

		assertTrue(dep01Com02.estaEnTexto("Comuna 02"));
		assertTrue(dep01Com02.estaEnTexto("atención"));
		assertTrue(dep02Com02.estaEnTexto("Comuna 02"));
		assertTrue(dep02Com02.estaEnTexto("atención"));
		assertTrue(dep03Com02.estaEnTexto("Comuna 02"));
		assertTrue(dep03Com02.estaEnTexto("atención"));
		assertTrue(dep04Com02.estaEnTexto("Comuna 02"));
		assertTrue(dep04Com02.estaEnTexto("atención"));

		assertTrue(dep01Com03.estaEnTexto("Comuna 03"));
		assertTrue(dep01Com03.estaEnTexto("atención"));
		assertTrue(dep02Com03.estaEnTexto("Comuna 03"));
		assertTrue(dep02Com03.estaEnTexto("atención"));
		assertTrue(dep03Com03.estaEnTexto("Comuna 03"));
		assertTrue(dep03Com03.estaEnTexto("atención"));

		assertFalse(dep01Com01.estaEnTexto("tp diseño"));
		assertFalse(dep02Com01.estaEnTexto("tp diseño"));
		assertFalse(dep03Com01.estaEnTexto("tp diseño"));

		assertFalse(dep01Com02.estaEnTexto("tp diseño"));
		assertFalse(dep02Com02.estaEnTexto("tp diseño"));
		assertFalse(dep03Com02.estaEnTexto("tp diseño"));
		assertFalse(dep04Com02.estaEnTexto("tp diseño"));

		assertFalse(dep01Com03.estaEnTexto("tp diseño"));
		assertFalse(dep02Com03.estaEnTexto("tp diseño"));
		assertFalse(dep03Com03.estaEnTexto("tp diseño"));
	}

}
