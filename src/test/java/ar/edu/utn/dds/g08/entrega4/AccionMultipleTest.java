package ar.edu.utn.dds.g08.entrega4;

import java.util.Arrays;

import ar.edu.utn.dds.g08.acciones.*;
import ar.edu.utn.dds.g08.busquedas.BuscadorRepositorio;
import ar.edu.utn.dds.g08.datos.CreadorRepositorioPDI;
import ar.edu.utn.dds.g08.fuentesDatos.RepositorioPDI;
import ar.edu.utn.dds.g08.perfilesUsuarios.*;

import org.junit.*;

import static org.junit.Assert.*;

public class AccionMultipleTest {
	RepositorioPDI repositorio = new CreadorRepositorioPDI().buildRepositorio();

	PerfilAdministrador administrador = new PerfilAdministrador("Administrador");

	PerfilTerminal terminal = new PerfilTerminal("terminal",
			new BuscadorRepositorio(repositorio));

	ObservadorBusqueda logueador = new LogueadorBusqueda();
	ObservadorBusqueda notDemora = new NotificadorDemora(administrador, 2000);

	@Test
	public void testProcesoMultiple() {
		Accion accionLocales = new ActualizacionLocalesComerciales(
				"/home/dds/git/2016ddsutng08/src/test/resources/actualizacionLocalesComercialesTest.txt",
				repositorio);
		accionLocales.setNotificador(new NotificadorVacio());

		AccionesUsuarios accionUsuarios = new AccionesUsuarios();
		accionUsuarios.setNotificador(new NotificadorVacio());
		accionUsuarios.addAccion(terminal, Arrays.asList(logueador, notDemora));

		assertTrue(terminal.getObservadores().size() == 0);
		assertTrue(repositorio.buscarPDI("Camisa").size() == 1);
		assertTrue(repositorio.buscarPDI("Chupin").size() == 0);

		AccionMultiple accionMultiple = new AccionMultiple();
		accionMultiple.addAccion(accionUsuarios);
		accionMultiple.addAccion(accionLocales);

		EstadoProceso estado = administrador.ejecutarProceso(accionMultiple);

		while (!estado.estaFinalizado())
			System.out.println("todavía no terminó");

		assertTrue(estado.finalizadoCorrectamente());
		assertTrue(repositorio.buscarPDI("Camisa").size() == 0);
		assertTrue(repositorio.buscarPDI("Chupin").size() == 1);
		assertTrue(terminal.getObservadores().size() == 2);
		assertTrue(terminal.getObservadores().contains(logueador));
		assertTrue(terminal.getObservadores().contains(notDemora));

	}
}
