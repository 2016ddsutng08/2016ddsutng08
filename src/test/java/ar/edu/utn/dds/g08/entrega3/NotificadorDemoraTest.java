package ar.edu.utn.dds.g08.entrega3;

import org.junit.Test;

import static org.mockito.Mockito.*;
import ar.edu.utn.dds.g08.busquedas.*;
import ar.edu.utn.dds.g08.datos.*;
import ar.edu.utn.dds.g08.perfilesUsuarios.*;
import ar.edu.utn.dds.g08.valueClasses.*;

public class NotificadorDemoraTest {
	BuscadorPDIs repositorio = new BuscadorRepositorio(
			new CreadorRepositorioPDI().buildRepositorio());

	GeneradorReportes reportes = new GeneradorReportes();

	PerfilAdministrador administrador = new PerfilAdministrador("Administrador");

	@Test
	public void testNotificadorDemora() {
		administrador.setMail("mail@diseño.com");

		NotificadorDemora notificador = new NotificadorDemora(administrador,
				10000);
		NotificadorDemora espiaNoti = spy(notificador);

		CreadorPerfilTerminal builderTerminal = new CreadorPerfilTerminal();

		builderTerminal.setRepositorio(repositorio);
		builderTerminal.setNotificadorDemora(espiaNoti);

		PerfilTerminal terminal = builderTerminal.buildTerminalNotificador();

		terminal.buscarPDI("Francés");

		verify(espiaNoti, times(0)).notificarDemoraExcesiva();

		notificador = new NotificadorDemora(administrador, -10000);
		espiaNoti = spy(notificador);

		builderTerminal.setNotificadorDemora(espiaNoti);

		terminal = builderTerminal.buildTerminalNotificador();

		terminal.buscarPDI("Francés");

		verify(espiaNoti, times(1)).notificarDemoraExcesiva();
	}

}
