package ar.edu.utn.dds.g08.entregaPersistencia;

import org.junit.*;

import static org.junit.Assert.*;
import ar.edu.utn.dds.g08.busquedas.*;
import ar.edu.utn.dds.g08.datos.*;
import ar.edu.utn.dds.g08.dominio.Parada;
import ar.edu.utn.dds.g08.fuentesDatos.RepositorioPDI;
import ar.edu.utn.dds.g08.perfilesUsuarios.*;
import ar.edu.utn.dds.g08.valueClasses.*;

import java.util.*;

import org.joda.time.*;

public class PerfilesTest extends PersistenciaTest {

	@Test
	public void testPerfilesUsuarios() {
		RepositorioPDI repositorio = new CreadorRepositorioPDI()
				.buildRepositorio();

		BuscadorPDIs buscadorRepo = new BuscadorRepositorio(repositorio);

		PerfilAdministrador administrador = new PerfilAdministrador(
				"Administrador");

		administrador.setContrasenia("pass");
		administrador.setMail("mail@mail.com");

		CreadorPerfilTerminal builderTerminal = new CreadorPerfilTerminal();

		builderTerminal.setRepositorio(buscadorRepo);
		builderTerminal.setGeneradorReportes(administrador.getReportes());

		PerfilTerminal terminal = builderTerminal.buildTerminalHistoriador();
		terminal.setContrasenia("pass terminal");

		terminal.buscarPDI("ICBC");

		withTransaction(() -> {
			repositorio.stream().filter(p -> p.getClass() == Parada.class)
					.forEach(p -> this.sesion.persist(p));
			repositorio.stream().filter(p -> p.getClass() != Parada.class)
					.forEach(p -> this.sesion.persist(p));
			this.sesion.persist(administrador);
			this.sesion.persist(terminal);
			administrador.getReportes().getBusquedas()
					.forEach(b -> this.sesion.persist(b));
		});

		this.sesion.clear();

		PerfilAdministrador administrador2 = this.sesion.find(
				PerfilAdministrador.class, administrador.getNombreUsuario());

		this.sesion
				.createQuery("from RegistroLogBusqueda",
						RegistroLogBusqueda.class).getResultList()
				.forEach(b -> administrador2.getReportes().addBusqueda(b));

		Map<LocalDate, Long> resultado = administrador2.busquedasPorFecha();
		LocalDate otro = new LocalDate();

		assertTrue(resultado.get(otro) == 1);

		PerfilTerminal terminal2 = this.sesion.find(PerfilTerminal.class,
				terminal.getNombreUsuario());

		Optional<ObservadorBusqueda> observador = terminal2.getObservadores()
				.stream().filter(o -> o.getClass() == HistorialBusqueda.class)
				.findFirst();

		if (observador.isPresent()) {
			HistorialBusqueda historial = (HistorialBusqueda) observador.get();

			historial.setGeneradorReportes(administrador2.getReportes());
		}

		terminal2.setRepositorio(new BuscadorRepositorio(repositorio));

		terminal2.buscarPDI("ICBC");

		resultado = administrador2.busquedasPorFecha();
		assertTrue(resultado.get(otro) == 2);

		this.sesion.clear();

		withTransaction(() -> {
			repositorio.stream().forEach(p -> this.sesion.delete(p));
			this.sesion.delete(administrador);
			this.sesion.delete(terminal);
			administrador.getReportes().getBusquedas()
					.forEach(b -> this.sesion.delete(b));
		});

		this.sesion.close();
	}
}
