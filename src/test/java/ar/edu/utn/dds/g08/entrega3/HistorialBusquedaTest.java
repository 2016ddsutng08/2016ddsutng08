package ar.edu.utn.dds.g08.entrega3;

import ar.edu.utn.dds.g08.busquedas.*;
import ar.edu.utn.dds.g08.datos.*;
import ar.edu.utn.dds.g08.perfilesUsuarios.*;

import org.junit.Test;
import static org.junit.Assert.*;

public class HistorialBusquedaTest {
	BuscadorPDIs repositorio = new BuscadorRepositorio(
			new CreadorRepositorioPDI().buildRepositorio());

	PerfilAdministrador administrador = new PerfilAdministrador("Administrador");

	@Test
	public void testHistorialBusqueda() {
		CreadorPerfilTerminal builderTerminal = new CreadorPerfilTerminal();

		builderTerminal.setRepositorio(repositorio);
		builderTerminal.setGeneradorReportes(administrador.getReportes());

		PerfilTerminal terminal = builderTerminal.buildTerminalHistoriador();

		terminal.buscarPDI("Francés");

		assertTrue(administrador.getReportes().cantidadBusquedas() == 1);

		terminal.buscarPDI("101");

		assertTrue(administrador.getReportes().cantidadBusquedas() == 2);

	}
}