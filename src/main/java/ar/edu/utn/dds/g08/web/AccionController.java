package ar.edu.utn.dds.g08.web;

import static spark.Spark.*;

import java.util.*;

import org.hibernate.Session;

import ar.edu.utn.dds.g08.persistencia.SessionFactorySingleton;
import ar.edu.utn.dds.g08.perfilesUsuarios.*;
import ar.edu.utn.dds.g08.valueClasses.EnumObservadoresBusqueda;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.template.handlebars.HandlebarsTemplateEngine;

public class AccionController extends AbstractControllerWeb {

	public static void init() {
		motorTemplates = new HandlebarsTemplateEngine();

		get("/acciones", AccionController::mostrarAcciones, motorTemplates);

		post("/acciones", AccionController::agregarAccion);

		delete("/acciones/:obs", AccionController::eliminarAccion);
	}

	public static ModelAndView mostrarAcciones(Request request,
			Response response) {
		String nombreUsuario = request.session().attribute("nombreUsuario");

		if (nombreUsuario == null) {
			response.redirect("/login");
			return new ModelAndView(new HashMap<>(), "login.hbs");
		}

		Map<String, Object> map = new HashMap<>();
		Session sesion = SessionFactorySingleton.getInstance().openSession();

		PerfilUsuario usuario = obtenerUsuario(sesion, nombreUsuario);

		List<ObservadorBusqueda> observadores = usuario.getObservadores();

		map.put("accions", EnumObservadoresBusqueda.values());
		map.put("observadors", observadores);

		sesion.close();

		return new ModelAndView(map, "acciones.hbs");
	}

	public static Object agregarAccion(Request request, Response response) {
		String nombreUsuario = request.session().attribute("nombreUsuario");

		if (nombreUsuario == null) {
			response.redirect("/login");
			return "";
		}

		Session sesion = SessionFactorySingleton.getInstance().openSession();

		PerfilUsuario usuario = obtenerUsuario(sesion, nombreUsuario);

		withTransaction(sesion, () -> {
			String param = request.queryParams("obs");

			if (EnumObservadoresBusqueda.HISTORIAL.toString().equals(param))
				usuario.agregarObservador(new HistorialBusqueda());
			else if (EnumObservadoresBusqueda.LOG.toString().equals(param))
				usuario.agregarObservador(new LogueadorBusqueda());
			else if (EnumObservadoresBusqueda.NOTIFICAR_DEMORA.toString()
					.equals(param))
				usuario.agregarObservador(new NotificadorDemora(
						(PerfilAdministrador) obtenerUsuario(sesion,
								"Administrador"), 3));

		});

		sesion.close();

		response.redirect("/acciones");

		return "";
	}

	public static Object eliminarAccion(Request request, Response response) {
		String nombreUsuario = request.session().attribute("nombreUsuario");

		if (nombreUsuario == null) {
			response.status(500);
		}

		Session sesion = SessionFactorySingleton.getInstance().openSession();

		PerfilUsuario usuario = obtenerUsuario(sesion, nombreUsuario);

		withTransaction(sesion, () -> {
			String param = request.params("obs");

			if (EnumObservadoresBusqueda.HISTORIAL.toString().equals(param))
				usuario.quitarObservador(HistorialBusqueda.class);
			else if (EnumObservadoresBusqueda.LOG.toString().equals(param))
				usuario.quitarObservador(LogueadorBusqueda.class);
			else if (EnumObservadoresBusqueda.NOTIFICAR_DEMORA.toString()
					.equals(param))
				usuario.quitarObservador(NotificadorDemora.class);
		});

		sesion.close();

		response.status(200);

		return "";
	}
}
