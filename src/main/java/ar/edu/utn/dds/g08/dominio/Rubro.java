package ar.edu.utn.dds.g08.dominio;

import ar.edu.utn.dds.g08.excepciones.NullParametro;

import javax.persistence.*;

@Entity
public class Rubro {

	@Id
	private String nombre;

	private double radio = 0;

	protected Rubro() {

	}

	public Rubro(String nombre, double radio) {
		this();

		if (nombre == null)
			throw new NullParametro("Ingrese valores no nulos");

		this.setRadio(radio);
		this.nombre = nombre;
	}

	public double getRadio() {
		return radio;
	}

	public void setRadio(double radio) {
		this.radio = radio;
	}

	public String getNombre() {
		return this.nombre;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Rubro))
			return false;

		Rubro rubro = (Rubro) obj;

		return this.nombre == rubro.getNombre();
	}

	@Override
	public int hashCode() {
		return this.nombre.hashCode();
	}

	@Override
	public String toString() {
		return nombre;
	}
}
