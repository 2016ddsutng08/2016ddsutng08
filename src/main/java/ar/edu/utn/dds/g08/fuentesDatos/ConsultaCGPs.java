package ar.edu.utn.dds.g08.fuentesDatos;

import java.util.List;

public interface ConsultaCGPs {
	public List<CentroDTO> buscarCGPs(String textoConsulta);
}