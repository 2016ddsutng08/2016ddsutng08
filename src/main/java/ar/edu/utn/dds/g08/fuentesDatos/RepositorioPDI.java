package ar.edu.utn.dds.g08.fuentesDatos;

import java.util.*;
import java.util.stream.*;

import ar.edu.utn.dds.g08.dominio.PuntoDeInteres;
import ar.edu.utn.dds.g08.excepciones.*;

public class RepositorioPDI {

	private Map<Integer, PuntoDeInteres> repositorio;

	private int contador;

	public RepositorioPDI() {
		repositorio = new HashMap<Integer, PuntoDeInteres>();
		contador = 0;
	}

	public Integer putPDI(PuntoDeInteres nuevoPDI) throws DuplicadoException {

		if (repositorio.containsValue(nuevoPDI))
			throw new DuplicadoException();

		repositorio.put(++contador, nuevoPDI);

		return contador;

	}

	public void removePDI(PuntoDeInteres pdi) throws InexistenteException {
		if (!this.contains(pdi))
			throw new InexistenteException("El PDI que quiere borrar no existe");

		repositorio.remove(this.getKey(pdi));

	}

	public void modifyPDI(PuntoDeInteres pdi) throws InexistenteException {
		if (!this.contains(pdi))
			throw new InexistenteException(
					"El PDI que quiere modificar no existe");

		repositorio.replace(this.getKey(pdi), pdi);

	}

	private int getKey(PuntoDeInteres pdi) throws InexistenteException { // esto
																			// hay
																			// q
																			// redefinirlo
		if (!this.contains(pdi))
			throw new InexistenteException("El PDI no existe");

		Set<Integer> set = repositorio.keySet();
		Iterator<Integer> it = set.iterator();
		PuntoDeInteres pdiRepo;
		int key;

		while (it.hasNext()) {
			key = it.next();
			pdiRepo = repositorio.get(key);
			if (pdi.equals(pdiRepo))
				return key;
		}

		return -1; // nunca llega aca
	}

	public List<PuntoDeInteres> getAll() {
		return repositorio.values().stream().collect(Collectors.toList());
	}

	public PuntoDeInteres buscarPDI(int key) {
		return repositorio.get(key);
	}

	public PuntoDeInteres buscarPDIPorId(int key) {
		Optional<PuntoDeInteres> opt = repositorio.values().stream()
				.filter(p -> p.getId() == key).findFirst();

		if (opt.isPresent())
			return opt.get();
		else
			return null;

	}

	public List<PuntoDeInteres> buscarPDI(String texto) {
		return repositorio.values().stream()
				.filter(pdi -> pdi.estaEnTexto(texto))
				.collect(Collectors.toList());
	}

	public List<PuntoDeInteres> buscarPDIPorTipo(String texto, Class<?> clase) {
		return repositorio.values().stream()
				.filter(pdi -> pdi.getClass() == clase)
				.filter(pdi -> pdi.estaEnTexto(texto))
				.collect(Collectors.toList());
	}

	public boolean contains(Object obj) {
		return repositorio.containsValue(obj);

	}

	public Stream<PuntoDeInteres> stream() {
		return repositorio.values().stream(); // stream de values
	}

}