package ar.edu.utn.dds.g08.busquedas;

import ar.edu.utn.dds.g08.dominio.*;
import ar.edu.utn.dds.g08.fuentesDatos.*;
import ar.edu.utn.dds.g08.valueClasses.*;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class BuscadorBancos implements BuscadorPDIs { // Es un ADAPTER

	ConsultaBancos adaptable;

	public BuscadorBancos(ConsultaBancos a) {
		adaptable = a;
	}

	@Override
	public PuntoDeInteres buscarPDI(Integer key) {
		return null;
	}

	@Override
	public PuntoDeInteres buscarPDIPorId(int key) {
		return null;
	}

	@Override
	public List<PuntoDeInteres> buscarPDI(String texto) {
		// TODO Auto-generated method stub
		return buscarPDI(texto, "");
	}

	private PuntoDeInteres adaptarPDI(JSONObject banco) {
		List<Servicio> serv = new ArrayList<Servicio>();

		banco.getJSONArray("servicios").forEach(
				s -> serv.add(this.adaptarServicio((String) s)));

		SucursalBanco nuevo = new SucursalBanco(new Direccion(
				new PointAdaptado(banco.getDouble("x"), banco.getDouble("y"))),
				serv, banco.getString("banco"), banco.getString("sucursal"));
		nuevo.setGerente(banco.getString("gerente"));

		return nuevo;
	}

	private Servicio adaptarServicio(String nombre) {
		Horario horario = new Horario(6, 7, 0, 0, 1, 0); // debe cambiar el
															// horario

		return new Servicio(nombre, Arrays.asList(horario));
	}

	@Override
	public List<PuntoDeInteres> buscarPDI(String texto1, String texto2) {
		JSONArray bancos;
		List<PuntoDeInteres> pdis = new ArrayList<PuntoDeInteres>();

		bancos = adaptable.buscarBancos(texto1, texto2);

		bancos.forEach(b -> pdis.add(this.adaptarPDI((JSONObject) b)));

		return pdis;
	}
}