package ar.edu.utn.dds.g08.perfilesUsuarios;

import ar.edu.utn.dds.g08.valueClasses.EnumObservadoresBusqueda;
import ar.edu.utn.dds.g08.valueClasses.GeneradorReportes;
import ar.edu.utn.dds.g08.valueClasses.RegistroLogBusqueda;

import javax.persistence.*;

@Entity
public class HistorialBusqueda extends ObservadorBusqueda {
	@Transient
	GeneradorReportes reportes = new GeneradorReportes();

	public HistorialBusqueda() {

	}

	public HistorialBusqueda(GeneradorReportes rep) {
		reportes = rep;
	}

	@Override
	public void actualizar(RegistroLogBusqueda log) {
		reportes.addBusqueda(log);
	}

	public GeneradorReportes getGeneradorReportes() {
		return reportes;
	}

	public void setGeneradorReportes(GeneradorReportes g) {
		reportes = g;
	}
	
	public String toString(){
		return EnumObservadoresBusqueda.HISTORIAL.toString();
	}
}
