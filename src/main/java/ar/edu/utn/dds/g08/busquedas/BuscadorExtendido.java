package ar.edu.utn.dds.g08.busquedas;

import ar.edu.utn.dds.g08.dominio.PuntoDeInteres;

import java.util.*;

public class BuscadorExtendido implements BuscadorPDIs {

	List<BuscadorPDIs> buscadores;

	public BuscadorExtendido() {

	}

	public BuscadorExtendido(List<BuscadorPDIs> b) {
		buscadores = b;
	}

	@Override
	public PuntoDeInteres buscarPDI(Integer key) {
		return null;
	}

	@Override
	public PuntoDeInteres buscarPDIPorId(int key) {
		return null;
	}

	@Override
	public List<PuntoDeInteres> buscarPDI(String texto) {
		// TODO Auto-generated method stub
		List<PuntoDeInteres> pdis = new ArrayList<PuntoDeInteres>();

		buscadores.stream().forEach(b -> pdis.addAll(b.buscarPDI(texto)));

		return pdis;
	}

	@Override
	public List<PuntoDeInteres> buscarPDI(String texto1, String texto2) {
		List<PuntoDeInteres> pdis = new ArrayList<PuntoDeInteres>();

		buscadores.stream().forEach(
				b -> pdis.addAll(b.buscarPDI(texto1, texto2)));

		return pdis;
	}

	public void addBuscador(BuscadorPDIs b) {
		this.buscadores.add(b);
	}

	public void setBuscadores(List<BuscadorPDIs> s) {
		this.buscadores = s;
	}
}
