package ar.edu.utn.dds.g08.web;

import static spark.Spark.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hibernate.Session;
import org.joda.time.*;
import org.joda.time.format.*;

import ar.edu.utn.dds.g08.busquedas.BuscadorPDIs;
import ar.edu.utn.dds.g08.perfilesUsuarios.PerfilAdministrador;
import ar.edu.utn.dds.g08.perfilesUsuarios.PerfilUsuario;
import ar.edu.utn.dds.g08.persistencia.SessionFactorySingleton;
import ar.edu.utn.dds.g08.valueClasses.RegistroLogBusqueda;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.template.handlebars.HandlebarsTemplateEngine;

public class HistorialController extends AbstractControllerWeb {

	public static void init() {
		motorTemplates = new HandlebarsTemplateEngine();

		get("/historial", HistorialController::mostrarHistorial, motorTemplates);

		get("/historial/:id", HistorialController::mostrarPDIs, motorTemplates);
	}

	public static ModelAndView mostrarHistorial(Request request,
			Response response) {
		String nombreUsuario = request.session().attribute("nombreUsuario");

		if (nombreUsuario == null) {
			response.redirect("/login");
			return new ModelAndView(new HashMap<>(), "login.hbs");
		}

		Map<String, Object> map = new HashMap<>();
		Session sesion = SessionFactorySingleton.getInstance().openSession();

		PerfilUsuario usuario = obtenerUsuario(sesion, nombreUsuario);

		if (!(usuario instanceof PerfilAdministrador)) {
			sesion.close();
			response.redirect("/login");
			return new ModelAndView(new HashMap<>(), "login.hbs");
		}

		PerfilAdministrador administrador = (PerfilAdministrador) usuario;
		List<RegistroLogBusqueda> busquedas = null;

		nombreUsuario = null;
		LocalDate fechaDesde = null;
		LocalDate fechaHasta = null;
		DateTimeFormatter formater = DateTimeFormat.forPattern("yyyy-mm-dd");

		nombreUsuario = request.queryParams("usuario");

		if (request.queryParams("fechaDesde") != null
				&& !request.queryParams("fechaDesde").isEmpty())
			fechaDesde = formater.parseLocalDate(request
					.queryParams("fechaDesde"));

		if (request.queryParams("fechaHasta") != null
				&& !request.queryParams("fechaHasta").isEmpty())
			fechaHasta = formater.parseLocalDate(request
					.queryParams("fechaHasta"));

		if (fechaDesde != null || fechaHasta != null) {

			administrador.setReportes(obtenerGeneradorReportes(sesion));

			if (nombreUsuario != null && !nombreUsuario.isEmpty())
				busquedas = administrador
						.getReportes()
						.resultadosEntreFechas(fechaDesde, fechaHasta)
						.stream()
						.filter(l -> l.getNombreUsuario().equals(
								request.queryParams("usuario")))
						.collect(Collectors.toList());
			else
				busquedas = administrador.getReportes().resultadosEntreFechas(
						fechaDesde, fechaHasta);

		} else if (nombreUsuario != null && !nombreUsuario.isEmpty()) {
			administrador.setReportes(obtenerGeneradorReportes(sesion));

			busquedas = administrador.getReportes().resultadosPorTerminal(
					nombreUsuario);
		}

		map.put("busquedas", busquedas);

		sesion.close();

		return new ModelAndView(map, "historial.hbs");
	}

	public static ModelAndView mostrarPDIs(Request request, Response response) {
		String nombreUsuario = request.session().attribute("nombreUsuario");

		if (nombreUsuario == null) {
			response.redirect("/login");
			return new ModelAndView(new HashMap<>(), "login.hbs");
		}

		Session sesion = SessionFactorySingleton.getInstance().openSession();

		PerfilUsuario usuario = obtenerUsuario(sesion, nombreUsuario);

		if (!(usuario instanceof PerfilAdministrador)) {
			sesion.close();
			response.redirect("/login");
			return new ModelAndView(new HashMap<>(), "login.hbs");
		}

		Map<String, Object> map = new HashMap<>();

		RegistroLogBusqueda busqueda = sesion.find(RegistroLogBusqueda.class,
				Integer.parseInt(request.params(":id")));

		BuscadorPDIs repo = obtenerBuscadorPDIs(sesion);

		map.put("pdis", repo.buscarPDI(busqueda.getFraseBuscada()));

		sesion.close();

		return new ModelAndView(map, "historialPDIs.hbs");
	}

}
