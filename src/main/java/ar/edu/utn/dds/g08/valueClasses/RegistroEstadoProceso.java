package ar.edu.utn.dds.g08.valueClasses;

import org.joda.time.*;
import static org.joda.time.Instant.now;

public class RegistroEstadoProceso {
	DateTime horaInicio;
	DateTime horaFin = null;
	String nombreProceso;
	String nombreUsuario;
	EnumEstadoProceso estado;
	String mensaje;

	public RegistroEstadoProceso(String nomProc, String nomUsu) {
		horaInicio = new DateTime(new Instant(now()));
		nombreProceso = nomProc;
		nombreUsuario = nomUsu;
		estado = EnumEstadoProceso.EN_PROCESO;
		mensaje = "";
	}

	public void setFinalizadoOk() {
		synchronized (this) {
			horaFin = new DateTime(new Instant(now()));
			estado = EnumEstadoProceso.FINALIZADO_OK;
		}
	}

	public void setFinalizadoError(String msj) {
		synchronized (this) {
			horaFin = new DateTime(new Instant(now()));
			estado = EnumEstadoProceso.FINALIZADO_ERROR;
			mensaje = msj;
		}
	}

	public DateTime getHoraInicio() {
		return horaInicio;
	}

	public DateTime getHoraFin() {
		return horaFin;
	}

	public String getNombreProceso() {
		return nombreProceso;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public EnumEstadoProceso getEstado() {
		return estado;
	}

	public String getMensaje() {
		return mensaje;
	}

	public boolean estaFinalizado() {
		return this.estado != EnumEstadoProceso.EN_PROCESO;
	}

	public boolean finalizadoCorrectamente() {
		return this.estado == EnumEstadoProceso.FINALIZADO_OK;
	}
}
