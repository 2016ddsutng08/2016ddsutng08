package ar.edu.utn.dds.g08.perfilesUsuarios;

import javax.persistence.*;

import ar.edu.utn.dds.g08.valueClasses.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class ObservadorBusqueda {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected Integer id;

	void actualizar(RegistroLogBusqueda log) {
	}
}
