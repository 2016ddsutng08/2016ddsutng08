package ar.edu.utn.dds.g08.dominio;

import java.util.List;
import java.util.Arrays;

import org.joda.time.Instant;

import ar.edu.utn.dds.g08.excepciones.NullParametro;

import javax.persistence.*;

@Entity
public class SucursalBanco extends PuntoDeInteres {

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
	protected List<Servicio> servicios;

	protected String banco; // va a ser una clase, mínimo con el icono y el
							// horario bancario
	protected String gerente;
	protected String nombreSucursal;

	@Transient
	public static final Horario HORARIO_BANCARIO = new Horario(1, 5, 10, 00,
			15, 00);

	public SucursalBanco() {

	}

	public SucursalBanco(Direccion direccion, List<Servicio> servicios,
			String banco, String nombre) {
		super("banco.icono", direccion);

		if (banco == null || servicios == null || nombre == null)
			throw new NullParametro(
					"Ingrese valores no nulos para banco, nombre  y serv");

		this.servicios = servicios;
		this.banco = banco;
		this.nombreSucursal = nombre;

		List<Horario> horarios = Arrays.asList(HORARIO_BANCARIO);

		this.servicios.stream().forEach(
				servicio -> servicio.setHorarios(horarios));
	}

	public List<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(List<Servicio> servicios) {
		this.servicios = servicios;
	}

	public boolean estaDisponible(Instant instante) {
		// se sabe que todos los servicios tienen el mismo horario
		return this.servicios.stream().anyMatch(
				servicio -> servicio.estaDisponible(instante));
	}

	@Override
	public boolean estaDisponible(Instant instante, String servicio) {
		Servicio servicioSel = this.getServicio(servicio);

		if (servicioSel == null)
			return false;
		else
			return servicioSel.estaDisponible(instante);
	}

	public Servicio getServicio(String nombre) {
		Servicio sel = null;

		for (Servicio servicio : this.servicios) {
			if (servicio.getNombre() == nombre)
				sel = servicio;
		}

		return sel;
	}

	@Override
	public boolean estaEnTexto(String texto) {
		return this.banco.startsWith(texto);
	}

	public String getGerente() {
		return this.gerente;
	}

	public void setGerente(String ger) {
		this.gerente = ger;
	}

	public String getNombreSucursal() {
		return this.nombreSucursal;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof SucursalBanco))
			return false;

		SucursalBanco banco = (SucursalBanco) obj;

		// Es igual si pertenece al mismo banco y mismo nombre de sucursal,
		// puede cambiar de dirección
		return this.banco.equals(banco.getBanco())
				&& (this.nombreSucursal == banco.getNombreSucursal());
	}

	@Override
	public int hashCode() {
		return this.banco.hashCode() + this.nombreSucursal.hashCode();
	}

	public void setDireccion(Direccion dir) {
		this.direccion = dir;
	}

	public String getBanco() {
		return this.banco;
	}

	@Override
	public String toString() {
		return this.banco.toString() + " " + this.nombreSucursal;
	}
}