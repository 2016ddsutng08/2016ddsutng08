package ar.edu.utn.dds.g08.perfilesUsuarios;

import org.joda.time.*;
import java.util.Map;
import java.util.List;

import ar.edu.utn.dds.g08.valueClasses.GeneradorReportes;
import ar.edu.utn.dds.g08.acciones.*;

import javax.persistence.*;

@Entity
public class PerfilAdministrador extends PerfilUsuario {
	@Transient
	protected GeneradorReportes reportes = new GeneradorReportes();

	@Transient
	protected EstadoProceso estadoProcesos;

	public PerfilAdministrador() {
		super();
	}

	public PerfilAdministrador(String nom) {
		super(nom);
	}

	public Map<LocalDate, Long> busquedasPorFecha() {
		return reportes.busquedasPorFecha();
	}

	public Map<LocalDate, Long> busquedasPorFechaPorTerminal(String terminal) {
		return reportes.busquedasPorFechaPorTerminal(terminal);
	}

	public Map<String, Integer> resultadosPorBusqueda() {
		return reportes.resultadosPorBusqueda();
	}

	public List<Integer> resultadosPorBusquedaPorTerminal(String terminal) {
		return reportes.resultadosPorBusquedaPorTerminal(terminal);
	}

	public EstadoProceso ejecutarProceso(Accion accionAEjecutar) {
		estadoProcesos = new EstadoProceso(this);

		estadoProcesos.setCantidadAcciones(1);

		new Thread(() -> accionAEjecutar.ejecutar(estadoProcesos)).start();

		return estadoProcesos;

	}

	public EstadoProceso deshacerProceso(Accion accionAEjecutar) {
		estadoProcesos = new EstadoProceso(this);

		estadoProcesos.setCantidadAcciones(1);

		new Thread(() -> accionAEjecutar.deshacer(estadoProcesos)).start();

		return estadoProcesos;

	}

	public GeneradorReportes getReportes() {
		return this.reportes;
	}

	public void setReportes(GeneradorReportes r) {
		this.reportes = r;
	}
}
