package ar.edu.utn.dds.g08.dominio;

import ar.edu.utn.dds.g08.excepciones.NullParametro;

import javax.persistence.*;

import ar.edu.utn.dds.g08.valueClasses.*;

@Embeddable
public class Direccion {

	protected String pais;
	protected String provincia;
	protected String localidad;
	protected String barrio;
	protected String calle;
	@Column(name = "altura")
	protected int numero;
	protected int piso;
	protected String depto;
	protected int unidad;
	protected String cp;
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	protected PointAdaptado ubicacion;

	public Direccion() {

	}

	public Direccion(PointAdaptado ubicacion) {
		this();

		if (ubicacion == null)
			throw new NullParametro("Ingrese valores no nulos");

		this.ubicacion = ubicacion;
	}

	public Direccion(String pais, String provincia, String localidad,
			String barrio, String calle, int numero, int piso, String depto,
			int unidad, String cp, PointAdaptado ubicacion) {
		this();

		if (calle == null)
			throw new NullParametro("Ingrese valores no nulos para calle");

		this.pais = pais;
		this.provincia = provincia;
		this.localidad = localidad;
		this.barrio = barrio;
		this.calle = calle;
		this.numero = numero;
		this.piso = piso;
		this.depto = depto;
		this.unidad = unidad;
		this.cp = cp;
		this.ubicacion = ubicacion;

	}

	public boolean esCercano(PointAdaptado ubicacion) {
		return (this.ubicacion.distance(ubicacion) < 0.5);
	}

	public double distancia(PointAdaptado ubicacion) {
		return this.ubicacion.distance(ubicacion);
	}

	public PointAdaptado getUbicacion() {
		return this.ubicacion;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Direccion))
			return false;

		Direccion dir = (Direccion) obj;

		return this.ubicacion.equals(dir.getUbicacion());

	}

	@Override
	public int hashCode() {
		return this.ubicacion.hashCode();
	}

	@Override
	public String toString() {
		if (this.calle == null || this.calle.isEmpty())
			return this.ubicacion.toString();
		else
			return this.calle;
	}
}
