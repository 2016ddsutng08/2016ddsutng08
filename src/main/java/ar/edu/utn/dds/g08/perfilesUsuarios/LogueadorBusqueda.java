package ar.edu.utn.dds.g08.perfilesUsuarios;

import ar.edu.utn.dds.g08.valueClasses.EnumObservadoresBusqueda;
import ar.edu.utn.dds.g08.valueClasses.RegistroLogBusqueda;

import org.apache.logging.log4j.*;

import javax.persistence.*;

@Entity
public class LogueadorBusqueda extends ObservadorBusqueda {

	@Transient
	private final static Logger logueo = LogManager.getLogger();

	@Override
	public void actualizar(RegistroLogBusqueda log) {
		logueo.info("Palabra buscada:" + log.getFraseBuscada()
				+ " Cantidad resultados:" + log.getCantidadResultados()
				+ " Demora en milisegundos:" + log.getMiliSegDemora());
	}

	public String toString(){
		return EnumObservadoresBusqueda.LOG.toString();
	}
}
