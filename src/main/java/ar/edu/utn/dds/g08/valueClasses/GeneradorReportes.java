package ar.edu.utn.dds.g08.valueClasses;

import java.util.*;
import java.util.stream.Collectors;
import org.joda.time.*;

public class GeneradorReportes {
	private List<RegistroLogBusqueda> busquedas = new ArrayList<RegistroLogBusqueda>();

	public void addBusqueda(RegistroLogBusqueda log) {
		busquedas.add(log);
	}

	public Map<LocalDate, Long> busquedasPorFecha() {
		return busquedas.stream().collect(
				Collectors.groupingBy(RegistroLogBusqueda::getFechaBusqueda,
						Collectors.counting()));
	}

	public Map<LocalDate, Long> busquedasPorFechaPorTerminal(String terminal) {
		return busquedas
				.stream()
				.filter(b -> b.getNombreUsuario() == terminal)
				.collect(
						Collectors.groupingBy(
								RegistroLogBusqueda::getFechaBusqueda,
								Collectors.counting()));
	}

	public Map<String, Integer> resultadosPorBusqueda() {
		return busquedas
				.stream()
				.collect(
						Collectors
								.groupingBy(
										RegistroLogBusqueda::getNombreUsuario,
										Collectors
												.summingInt(RegistroLogBusqueda::getCantidadResultados)));
	}

	public List<Integer> resultadosPorBusquedaPorTerminal(String terminal) {
		return busquedas.stream().filter(b -> b.getNombreUsuario() == terminal)
				.map(RegistroLogBusqueda::getCantidadResultados)
				.collect(Collectors.toList());
	}

	public List<RegistroLogBusqueda> resultadosPorTerminal(String terminal) {
		return busquedas.stream()
				.filter(b -> b.getNombreUsuario().equals(terminal))
				.collect(Collectors.toList());
	}

	public List<RegistroLogBusqueda> resultadosEntreFechas(LocalDate desde,
			LocalDate hasta) {
		// no pueden ser ambos null
		if (desde != null && hasta != null)
			return busquedas
					.stream()
					.filter(b -> b.getFechaBusqueda().compareTo(desde) >= 0
							&& b.getFechaBusqueda().compareTo(hasta) <= 0)
					.collect(Collectors.toList());
		else if (desde != null && hasta == null)
			return busquedas.stream()
					.filter(b -> b.getFechaBusqueda().compareTo(desde) >= 0)
					.collect(Collectors.toList());
		else if (desde == null && hasta != null)
			return busquedas.stream()
					.filter(b -> b.getFechaBusqueda().compareTo(hasta) <= 0)
					.collect(Collectors.toList());

		return null;
	}

	public int cantidadBusquedas() {
		return busquedas.size();
	}

	public List<RegistroLogBusqueda> getBusquedas() {
		return busquedas;
	}
}
