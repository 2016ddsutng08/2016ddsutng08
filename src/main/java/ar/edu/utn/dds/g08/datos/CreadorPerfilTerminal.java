package ar.edu.utn.dds.g08.datos;

import ar.edu.utn.dds.g08.busquedas.*;
import ar.edu.utn.dds.g08.perfilesUsuarios.*;
import ar.edu.utn.dds.g08.valueClasses.GeneradorReportes;

public class CreadorPerfilTerminal {
	BuscadorPDIs repositorio;
	ObservadorBusqueda logueador;
	ObservadorBusqueda notificador;
	ObservadorBusqueda historiador;

	public void setRepositorio(BuscadorPDIs repo) {
		repositorio = repo;
	}

	public void setLogueador(LogueadorBusqueda log) {
		logueador = log;
	}

	public void setGeneradorReportes(GeneradorReportes rep) {
		historiador = new HistorialBusqueda(rep);
	}

	public void setNotificadorDemora(NotificadorDemora noti) {
		notificador = noti;
	}

	public PerfilTerminal buildTerminalLogueador() {
		PerfilTerminal nuevoTerminal = new PerfilTerminal("Usuario Logueador",
				repositorio);

		nuevoTerminal.agregarObservador(logueador);

		return nuevoTerminal;
	}

	public PerfilTerminal buildTerminalNotificador() {
		PerfilTerminal nuevoTerminal = new PerfilTerminal("Usuario Logueador",
				repositorio);

		nuevoTerminal.agregarObservador(notificador);

		return nuevoTerminal;
	}

	public PerfilTerminal buildTerminalHistoriador() {
		PerfilTerminal nuevoTerminal = new PerfilTerminal("Usuario Logueador",
				repositorio);

		nuevoTerminal.agregarObservador(historiador);

		return nuevoTerminal;
	}

	public PerfilTerminal buildTerminalCompleto(String nom) {
		PerfilTerminal nuevoTerminal = new PerfilTerminal(nom, repositorio);

		nuevoTerminal.agregarObservador(logueador);
		nuevoTerminal.agregarObservador(notificador);
		nuevoTerminal.agregarObservador(historiador);

		return nuevoTerminal;
	}

	public PerfilTerminal buildTerminalVacio() {
		return new PerfilTerminal("Terminal Vacío", repositorio);
	}
}
