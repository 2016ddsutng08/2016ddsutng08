package ar.edu.utn.dds.g08.valueClasses;

public enum EnumEstadoProceso {
	EN_PROCESO("En Proceso"), FINALIZADO_OK("Finalilzado OK"), FINALIZADO_ERROR(
			"Finalizado Error");

	private final String texto;

	private EnumEstadoProceso(final String t) {
		this.texto = t;
	}

	@Override
	public String toString() {
		return texto;
	}
}
