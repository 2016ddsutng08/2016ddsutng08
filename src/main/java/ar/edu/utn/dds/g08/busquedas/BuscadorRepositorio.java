package ar.edu.utn.dds.g08.busquedas;

import ar.edu.utn.dds.g08.fuentesDatos.*;
import ar.edu.utn.dds.g08.dominio.*;

import java.util.*;

public class BuscadorRepositorio implements BuscadorPDIs { // Es un ADAPTER

	RepositorioPDI adaptable;

	public BuscadorRepositorio(RepositorioPDI repo) {
		adaptable = repo;
	}

	@Override
	public PuntoDeInteres buscarPDI(Integer key) {
		return adaptable.buscarPDI(key);
	}

	@Override
	public PuntoDeInteres buscarPDIPorId(int key) {
		return adaptable.buscarPDIPorId(key);
	}

	@Override
	public List<PuntoDeInteres> buscarPDI(String texto) {
		// TODO Auto-generated method stub
		return adaptable.buscarPDI(texto);
	}

	@Override
	public List<PuntoDeInteres> buscarPDI(String texto1, String texto2) {
		return buscarPDI(texto1);
	}

	public void setRepositorio(RepositorioPDI r) {
		this.adaptable = r;
	}
}