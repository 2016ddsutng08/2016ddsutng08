package ar.edu.utn.dds.g08.acciones;

import java.io.*;
import java.nio.file.*;
import java.util.*;

import ar.edu.utn.dds.g08.dominio.*;
import ar.edu.utn.dds.g08.excepciones.AccionException;
import ar.edu.utn.dds.g08.fuentesDatos.*;

public class ActualizacionLocalesComerciales extends Accion {
	Path path;
	RepositorioPDI repositorio;

	public ActualizacionLocalesComerciales(String p, RepositorioPDI r) {
		path = Paths.get(p);
		repositorio = r;
	}

	@Override
	public void ejecutarProcesoPropio() throws Exception {
		BufferedReader lector = Files.newBufferedReader(path);

		this.procesarArchivo(lector);
	}

	@Override
	public void deshacerProcesoPropio() throws Exception {
		throw new AccionException("no se puede deshacer");
	}

	protected void procesarArchivo(BufferedReader archivo) throws IOException {
		String linea = null;
		String[] splitLinea = null;

		while ((linea = archivo.readLine()) != null) {
			splitLinea = linea.split(";");

			this.actualizarPDIs(splitLinea[0], splitLinea[1]);
		}

	}

	protected void actualizarPDIs(String nombreFantasia, String palabrasClave) {
		List<PuntoDeInteres> listaLocales = null;
		LocalComercial local;
		Iterator<PuntoDeInteres> iterador;

		listaLocales = repositorio.buscarPDIPorTipo(nombreFantasia,
				LocalComercial.class);

		if (listaLocales.isEmpty()) {
			local = new LocalComercial("icono", new Direccion(), null,
					Arrays.asList(palabrasClave.split(" ")), null,
					nombreFantasia);

			repositorio.putPDI(local);
		} else {
			iterador = listaLocales.iterator();

			while (iterador.hasNext()) {
				local = (LocalComercial) iterador.next();
				local.setPalabrasClaves(Arrays.asList(palabrasClave.split(" ")));
				repositorio.modifyPDI(local); // esto esta de mas, pero lo uso
												// xq es lo q se pidio en la ent
												// 2
			}
		}
	}
}
