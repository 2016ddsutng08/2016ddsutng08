package ar.edu.utn.dds.g08.datos;

import java.util.Arrays;
import java.util.List;

import ar.edu.utn.dds.g08.dominio.Colectivo;
import ar.edu.utn.dds.g08.dominio.Direccion;
import ar.edu.utn.dds.g08.dominio.Parada;
import ar.edu.utn.dds.g08.valueClasses.*;

public class CreadorColectivo {
	public Parada paradaCampus = new CreadorParada().buildParada1();
	public Parada paradaRetiro = new CreadorParada().buildParada2();
	public Parada paradaConstitucion = new CreadorParada().buildParada3();
	public Parada paradaBoedo = new CreadorParada().buildParada4();
	public Parada paradaDevoto = new CreadorParada().buildParada5();
	public Parada paradaPalermo = new CreadorParada().buildParada6();
	public Parada paradaPteSaavedra = new CreadorParada().buildParada7();
	public Parada paradaMedrano = new CreadorParada().buildParada8();
	
	List<Parada> paradas114 = Arrays.asList(paradaCampus, paradaDevoto);
	List<Parada> paradas101 = Arrays.asList(paradaCampus, paradaConstitucion,
			paradaRetiro);
	List<Parada> paradas152 = Arrays.asList(paradaRetiro, paradaPalermo,
			paradaPteSaavedra);
	List<Parada> paradas115 = Arrays.asList(paradaBoedo, paradaConstitucion,
			paradaRetiro, paradaPalermo, paradaPteSaavedra);
	List<Parada> paradas55 = Arrays.asList(paradaBoedo, paradaPalermo);
	List<Parada> paradas60 = Arrays.asList(paradaConstitucion, paradaRetiro,
			paradaPalermo, paradaPteSaavedra);
	List<Parada> paradas71 = Arrays.asList(paradaPteSaavedra, paradaMedrano,
			paradaPalermo);
	List<Parada> paradas160 = Arrays.asList(paradaRetiro, paradaMedrano,
			paradaPalermo, paradaPteSaavedra);
	List<Parada> paradas7 = Arrays.asList(paradaCampus, paradaConstitucion,
			paradaRetiro, paradaPalermo);
	List<Parada> paradasTotal = Arrays.asList(paradaCampus, paradaBoedo,
			paradaConstitucion, paradaRetiro, paradaMedrano, paradaPalermo,
			paradaDevoto, paradaPteSaavedra);

	public Colectivo buildColectivo1() {
		return new Colectivo("icono 114", new Direccion(new PointAdaptado(-34.000000,
				-58.000000)), "114", paradas114);
	}

	public Colectivo buildColectivo2() {
		return new Colectivo("icono 101", new Direccion(new PointAdaptado(-34.000000,
				-58.000000)), "101", paradas101);
	}

	public Colectivo buildColectivo3() {
		return new Colectivo("icono 152", new Direccion(new PointAdaptado(-34.000000,
				-58.000000)), "152", paradas152);
	}

	public Colectivo buildColectivo4() {
		return new Colectivo("icono 115", new Direccion(new PointAdaptado(-34.000000,
				-58.000000)), "115", paradas115);
	}

	public Colectivo buildColectivo5() {
		return new Colectivo("icono 55", new Direccion(new PointAdaptado(-34.000000,
				-58.000000)), "55", paradas55);
	}

	public Colectivo buildColectivo6() {
		return new Colectivo("icono 60", new Direccion(new PointAdaptado(-34.000000,
				-58.000000)), "60", paradas60);
	}

	public Colectivo buildColectivo7() {
		return new Colectivo("icono 71", new Direccion(new PointAdaptado(-34.000000,
				-58.000000)), "71", paradas71);
	}

	public Colectivo buildColectivo8() {
		return new Colectivo("icono 160", new Direccion(new PointAdaptado(-34.000000,
				-58.000000)), "160", paradas160);
	}

	public Colectivo buildColectivo9() {
		return new Colectivo("icono 7", new Direccion(new PointAdaptado(-34.000000,
				-58.000000)), "7", paradas7);
	}

	public Colectivo buildColectivo10() {
		return new Colectivo("icono Total", new Direccion(new PointAdaptado(-34.000000,
				-58.000000)), "Total", paradasTotal);
	}
}