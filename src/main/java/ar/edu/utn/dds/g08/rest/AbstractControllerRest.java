package ar.edu.utn.dds.g08.rest;

import spark.Spark;
import spark.Route;

import com.fasterxml.jackson.databind.*;
import java.io.*;

public abstract class AbstractControllerRest {

	public String dataToJson(Object data) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			StringWriter sw = new StringWriter();
			mapper.writeValue(sw, data);
			return sw.toString();
		} catch (IOException e) {
			throw new RuntimeException("IOException from a StringWriter?");
		}
	}

	public void get(final String path, final Route route) {
		Spark.get(path, (request, response) -> {
			response.status(200);
			response.type("application/json");
			return dataToJson(route.handle(request, response));
		});
	}

	public void post(final String path, final Route route) {
		Spark.post(path, (request, response) -> {
			response.status(200);
			response.type("application/json");
			return dataToJson(route.handle(request, response));
		});
	}

	public void delete(final String path, final Route route) {
		Spark.delete(path, (request, response) -> {
			response.status(200);
			response.type("application/json");
			return dataToJson(route.handle(request, response));
		});
	}

	public void put(final String path, final Route route) {
		Spark.put(path, (request, response) -> {
			response.status(200);
			response.type("application/json");
			return dataToJson(route.handle(request, response));
		});
	}
}