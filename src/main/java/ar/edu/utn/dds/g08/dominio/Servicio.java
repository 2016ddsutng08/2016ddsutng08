package ar.edu.utn.dds.g08.dominio;

import org.joda.time.Instant;

import ar.edu.utn.dds.g08.excepciones.NullParametro;

import java.util.List;

import javax.persistence.*;

@Entity
public class Servicio {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected int id;

	protected String nombre;

	@ElementCollection(fetch = FetchType.EAGER)
	protected List<Horario> horarios;

	public Servicio() {

	}

	public Servicio(String nombre, List<Horario> horarios) {
		if (nombre == null || horarios == null)
			throw new NullParametro("Ingrese valores no nulos");

		this.nombre = nombre;
		this.setHorarios(horarios);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String n) {
		this.nombre = n;
	}

	public List<Horario> getHorarios() {
		return this.horarios;
	}

	public void setHorarios(List<Horario> horarios) {
		this.horarios = horarios;
	}

	public boolean estaDisponible(Instant instante) {
		return this.horarios.stream().anyMatch(
				horario -> horario.estaDisponible(instante));
	}
}
