package ar.edu.utn.dds.g08.acciones;

import ar.edu.utn.dds.g08.valueClasses.RegistroEstadoProceso;

public class Reintentar implements Notificador {

	int reintentos;
	Accion accionAsociada;

	@Override
	public void notificar(EstadoProceso est, RegistroEstadoProceso reg) {
		if (reintentos > 0) {
			reintentos--;

			est.setCantidadAcciones(est.getCantidadAcciones() + 1);

			reg.setFinalizadoError("Error en procesar Acción");
			accionAsociada.ejecutar(est);
		} else
			reg.setFinalizadoError("Error en procesar Acción");

	}

	public void setReintentos(int n) {
		reintentos = n;
	}

	public void setAccionAsociada(Accion a) {
		accionAsociada = a;
	}
}
