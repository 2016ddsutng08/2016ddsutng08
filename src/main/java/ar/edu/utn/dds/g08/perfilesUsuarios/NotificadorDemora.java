package ar.edu.utn.dds.g08.perfilesUsuarios;

import ar.edu.utn.dds.g08.valueClasses.EnumObservadoresBusqueda;
import ar.edu.utn.dds.g08.valueClasses.RegistroLogBusqueda;

import javax.persistence.*;

@Entity
public class NotificadorDemora extends ObservadorBusqueda {
	@ManyToOne(fetch = FetchType.EAGER)
	protected PerfilAdministrador administrador;

	protected long demoraTolerable;

	public NotificadorDemora() {

	}

	public NotificadorDemora(PerfilAdministrador adm, long dem) {
		administrador = adm;
		demoraTolerable = dem; // en milisegundos
	}

	@Override
	public void actualizar(RegistroLogBusqueda log) {
		// TODO Auto-generated method stub

		this.controlarDemora(log.getMiliSegDemora());
	}

	protected void controlarDemora(long miliSeg) {
		if (miliSeg > demoraTolerable)
			this.notificarDemoraExcesiva();
	}

	public void notificarDemoraExcesiva() {
		System.out.println("Se envía mail al administrador: "
				+ administrador.getMail());
		/*
		 * Properties props = new Properties();
		 * 
		 * // Nombre del host de correo, es smtp.gmail.com
		 * props.setProperty("mail.smtp.host", "smtp.gmail.com");
		 * 
		 * // TLS si está disponible
		 * props.setProperty("mail.smtp.starttls.enable", "true");
		 * 
		 * // Puerto de gmail para envio de correos
		 * props.setProperty("mail.smtp.port", "25");
		 * 
		 * // Nombre del usuario props.setProperty("mail.smtp.user",
		 * "jmatiasschneider@gmail.com");
		 * 
		 * // Si requiere o no usuario y password para conectarse.
		 * props.setProperty("mail.smtp.auth", "true");
		 * 
		 * try { Session session = Session.getDefaultInstance(props);
		 * 
		 * // Para obtener un log de salida más extenso //
		 * session.setDebug(true);
		 * 
		 * MimeMessage message = new MimeMessage(session);
		 * 
		 * // Quien envia el correo message.setFrom(new
		 * InternetAddress("jmatiasschneider@gmail.com"));
		 * 
		 * // A quien va dirigido message.addRecipient(Message.RecipientType.TO,
		 * new InternetAddress( "jmatiasschneider@gmail.com"));
		 * 
		 * message.setSubject("Asunto del mensaje");
		 * message.setText("Texto del mensaje");
		 * 
		 * Transport t = session.getTransport("smtp");
		 * 
		 * // Aqui usuario y password de gmail
		 * t.connect("jmatiasschneider@gmail.com", "M3035T5731S");
		 * t.sendMessage(message, message.getAllRecipients()); t.close(); }
		 * catch (MessagingException e) { e.printStackTrace(); }
		 */
	}

	public String toString() {
		return EnumObservadoresBusqueda.NOTIFICAR_DEMORA.toString();
	}

	public PerfilAdministrador getPerfilAdministrador() {
		return this.administrador;
	}

	public long getDemoraTolerable() {
		return this.demoraTolerable;
	}

	public void setPerfilAdministrador(PerfilAdministrador a) {
		this.administrador = a;
	}

	public void getDemoraTolerable(long d) {
		this.demoraTolerable = d;
	}

}
