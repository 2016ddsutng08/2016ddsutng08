package ar.edu.utn.dds.g08.acciones;

import java.util.*;

import ar.edu.utn.dds.g08.perfilesUsuarios.*;

public class AccionesUsuarios extends Accion {
	Map<PerfilTerminal, List<ObservadorBusqueda>> accionesAnteriores = new HashMap<PerfilTerminal, List<ObservadorBusqueda>>();
	Map<PerfilTerminal, List<ObservadorBusqueda>> accionesNuevas = new HashMap<PerfilTerminal, List<ObservadorBusqueda>>();

	@Override
	public void ejecutarProcesoPropio() throws Exception {
		setAccionesAnteriores();

		ejecutarAccionesNuevas();
	}

	@Override
	public void deshacerProcesoPropio() {
		ejecutarAccionesAnteriores();

		accionesAnteriores = accionesNuevas;

	}

	public void setAcciones(Map<PerfilTerminal, List<ObservadorBusqueda>> nueva) {
		accionesNuevas = nueva;
	}

	public void addAccion(PerfilTerminal terminal,
			List<ObservadorBusqueda> acciones) {
		accionesNuevas.put(terminal, acciones);
	}

	protected void setAccionesAnteriores() {
		accionesAnteriores.clear();

		accionesNuevas.keySet().forEach(
				perfil -> accionesAnteriores.put(perfil,
						perfil.getObservadores()));
	}

	protected void ejecutarAccionesNuevas() {
		accionesNuevas.keySet().forEach(
				terminal -> terminal.setObservadores(accionesNuevas
						.get(terminal)));
	}

	protected void ejecutarAccionesAnteriores() {
		accionesAnteriores.keySet().forEach(
				terminal -> terminal.setObservadores(accionesAnteriores
						.get(terminal)));
	}
}
