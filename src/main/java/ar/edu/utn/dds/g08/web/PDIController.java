package ar.edu.utn.dds.g08.web;

import static spark.Spark.*;
import ar.edu.utn.dds.g08.dominio.*;
import ar.edu.utn.dds.g08.perfilesUsuarios.HistorialBusqueda;
import ar.edu.utn.dds.g08.perfilesUsuarios.ObservadorBusqueda;
import ar.edu.utn.dds.g08.perfilesUsuarios.PerfilUsuario;
import ar.edu.utn.dds.g08.persistencia.SessionFactorySingleton;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.template.handlebars.HandlebarsTemplateEngine;

import java.util.*;

import org.hibernate.Session;

public class PDIController extends AbstractControllerWeb {

	public static void init() {
		motorTemplates = new HandlebarsTemplateEngine();

		get("/pdis", PDIController::mostrarPDIs, motorTemplates);

		get("/pdis/:id", PDIController::mostrarPDI, motorTemplates);
	}

	public static ModelAndView mostrarPDIs(Request request, Response response) {
		String nombreUsuario = request.session().attribute("nombreUsuario");

		if (nombreUsuario == null) {
			response.redirect("/login");
			return new ModelAndView(new HashMap<>(), "login.hbs");
		}

		Map<String, Object> map = new HashMap<>();
		Session sesion = SessionFactorySingleton.getInstance().openSession();

		PerfilUsuario usuario = obtenerUsuario(sesion, nombreUsuario);

		if (request.queryParams("fraseBuscada") != null
				&& !request.queryParams("fraseBuscada").isEmpty()) {

			usuario.setRepositorio(obtenerBuscadorPDIs(sesion));

			map.put("words", Arrays.asList(request.queryParams("fraseBuscada")));

			map.put("pdis",
					usuario.buscarPDI(request.queryParams("fraseBuscada")));
		}

		Optional<ObservadorBusqueda> observador = usuario.getObservadores()
				.stream().filter(o -> o.getClass() == HistorialBusqueda.class)
				.findFirst();

		if (observador.isPresent()) {
			HistorialBusqueda historial = (HistorialBusqueda) observador.get();

			withTransaction(sesion,
					() -> {
						historial.getGeneradorReportes().getBusquedas()
								.forEach(b -> sesion.persist(b));
					});
		}

		sesion.close();

		return new ModelAndView(map, "pdis.hbs");
	}

	public static ModelAndView mostrarPDI(Request request, Response response) {
		String nombreUsuario = request.session().attribute("nombreUsuario");

		if (nombreUsuario == null) {
			response.redirect("/login");
			return new ModelAndView(new HashMap<>(), "login.hbs");
		}

		Map<String, Object> map = new HashMap<>();
		Session sesion = SessionFactorySingleton.getInstance().openSession();

		PuntoDeInteres pdi = obtenerBuscadorPDIs(sesion).buscarPDIPorId(
				Integer.parseInt(request.params(":id")));

		String template = "";

		if (pdi.getClass() == Parada.class) {
			template = "parada.hbs";
			Parada parada = (Parada) pdi;
			map.put("lineas", parada.getLineas());
		} else if (pdi.getClass() == CGP.class) {
			template = "cgp.hbs";

			CGP cgp = (CGP) pdi;
			map.put("servicios", cgp.getServicios());
		} else if (pdi.getClass() == Colectivo.class)
			template = "colectivo.hbs";
		else if (pdi.getClass() == LocalComercial.class)
			template = "localComercial.hbs";
		else if (pdi.getClass() == SucursalBanco.class) {
			template = "sucursalBanco.hbs";

			SucursalBanco banco = (SucursalBanco) pdi;
			map.put("servicios", banco.getServicios());
		}

		map.put("pdi", pdi);

		sesion.close();

		return new ModelAndView(map, template);

	}
}
