package ar.edu.utn.dds.g08.valueClasses;

import java.util.List;
import java.util.stream.Collectors;

import org.uqbar.geodds.*;

import javax.persistence.*;

@Embeddable
public class PolygonAdaptado {
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	private List<PointAdaptado> superficie;

	@Transient
	private Polygon poligono;

	public PolygonAdaptado() {
		poligono = new Polygon();
	}

	public PolygonAdaptado(List<PointAdaptado> puntos) {
		List<Point> punto = puntos.stream().map(p -> p.getPoint())
				.collect(Collectors.toList());

		poligono = new Polygon(punto);

		superficie = puntos;

	}

	public boolean add(final PointAdaptado point) {
		superficie.add(point);

		return this.poligono.add(point.getPoint());
	}

	public boolean isInside(final PointAdaptado point) {
		return this.poligono.isInside(point.getPoint());
	}

	public List<PointAdaptado> getSuperficie() {
		return superficie;
	}
}
