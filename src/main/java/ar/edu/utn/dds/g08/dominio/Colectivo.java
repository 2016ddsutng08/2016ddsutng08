package ar.edu.utn.dds.g08.dominio;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ar.edu.utn.dds.g08.excepciones.NullParametro;
import ar.edu.utn.dds.g08.valueClasses.*;

import javax.persistence.*;

@Entity
public class Colectivo extends PuntoDeInteres {

	@OneToMany(mappedBy = "colectivo", cascade = CascadeType.ALL, orphanRemoval = false)
	public List<ParadaColectivo> paradas = new ArrayList<ParadaColectivo>(); // tiene
																				// q
																				// estar
																				// ordenada,
																				// para
																				// saber
																				// el
	// recorrido

	protected String linea;

	public Colectivo() {

	}

	public Colectivo(String icono, Direccion direccion, String linea,
			List<Parada> paradas) {
		super(icono, direccion); // la dirección es la terminal

		if (linea == null || paradas == null)
			throw new NullParametro("Ingrese valores no nulos");

		this.linea = linea;
		this.setParadas(paradas);
	}

	public List<Parada> getParadas() {
		return this.paradas.stream().map(pc -> pc.getParada())
				.collect(Collectors.toList());
	}

	public void setParadas(List<Parada> paradas) {
		paradas.forEach(p -> this.paradas.add(new ParadaColectivo(p, this)));
	}

	@Override
	public boolean esCercano(PointAdaptado unPunto) {
		return this.paradas.stream().map(pc -> pc.getParada())
				.anyMatch(parada -> parada.esCercano(unPunto));
	}

	public String getLinea() {
		return this.linea;
	}

	@Override
	public boolean estaEnTexto(String texto) {
		return this.linea.equals(texto);
	}

	public void addParada(Parada p) {
		this.paradas.add(new ParadaColectivo(p, this));
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Colectivo))
			return false;

		Colectivo cole = (Colectivo) obj;

		// son iguales cuando tienen = numero y las mismas paradas
		return (this.linea == cole.linea)
				&& this.getParadas().equals(cole.getParadas());
	}

	@Override
	public int hashCode() {
		List<Parada> lista = this.getParadas();

		int hash = lista.hashCode();
		return this.linea.hashCode() + hash;
	}

	@Override
	public String toString() {
		return "Colectivo Línea" + " " + this.linea;
	}
}
