package ar.edu.utn.dds.g08.excepciones;

public class AccionException extends RuntimeException {

	public AccionException(String mensaje) {
		super(mensaje);
	}
}
