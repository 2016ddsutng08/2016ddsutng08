package ar.edu.utn.dds.g08.web;

import static spark.Spark.get;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;

import ar.edu.utn.dds.g08.perfilesUsuarios.PerfilAdministrador;
import ar.edu.utn.dds.g08.perfilesUsuarios.PerfilUsuario;
import ar.edu.utn.dds.g08.persistencia.SessionFactorySingleton;
import ar.edu.utn.dds.g08.valueClasses.EnumAccionesUsuario;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.template.handlebars.HandlebarsTemplateEngine;

public class MenuController extends AbstractControllerWeb {

	public static void init() {
		motorTemplates = new HandlebarsTemplateEngine();

		get("/menu", MenuController::mostrarMenu, motorTemplates);
	}

	public static ModelAndView mostrarMenu(Request request, Response response) {
		String nombreUsuario = request.session().attribute("nombreUsuario");

		if (nombreUsuario == null) {
			response.redirect("/login");
			return new ModelAndView(new HashMap<>(), "login.hbs");
		}

		Session sesion = SessionFactorySingleton.getInstance().openSession();

		PerfilUsuario usuario = obtenerUsuario(sesion, nombreUsuario);

		List<EnumAccionesUsuario> acciones;

		if (usuario.getClass() == PerfilAdministrador.class)
			acciones = Arrays.asList(EnumAccionesUsuario.BUSCAR_PDIS,
					EnumAccionesUsuario.ACCIONES_BUSQUEDAS,
					EnumAccionesUsuario.REPORTES);
		else
			acciones = Arrays.asList(EnumAccionesUsuario.BUSCAR_PDIS,
					EnumAccionesUsuario.ACCIONES_BUSQUEDAS);

		Map<String, Object> map = new HashMap<>();

		map.put("usuario", usuario);
		map.put("accions", acciones);

		sesion.close();

		return new ModelAndView(map, "menu.hbs");

	}

}
