package ar.edu.utn.dds.g08.perfilesUsuarios;

import java.util.*;

import javax.persistence.*;

import org.joda.time.Instant;

import ar.edu.utn.dds.g08.busquedas.BuscadorPDIs;
import ar.edu.utn.dds.g08.dominio.PuntoDeInteres;
import ar.edu.utn.dds.g08.valueClasses.RegistroLogBusqueda;

//import org.hibernate.annotations.ColumnTransformer;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class PerfilUsuario {
	@Id
	protected String nombreUsuario;

	protected String email = "";

	@Column(name = "password")
	protected String contrasenia;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
	private List<ObservadorBusqueda> observadores = new ArrayList<ObservadorBusqueda>();

	@Transient
	protected BuscadorPDIs repositorio;

	public PerfilUsuario() {

	}

	public PerfilUsuario(String nombre) {
		this();

		nombreUsuario = nombre;
	}

	public void setMail(String mail) {
		email = mail;
	}

	public String getMail() {
		return email;
	}

	public void setContrasenia(String c) {
		this.contrasenia = c;
	}

	public String getContrasenia() {
		return this.contrasenia;
	}

	public List<PuntoDeInteres> buscarPDI(String texto) {
		long demora = System.currentTimeMillis();

		List<PuntoDeInteres> resultado = repositorio.buscarPDI(texto);

		demora -= System.currentTimeMillis();

		RegistroLogBusqueda log = new RegistroLogBusqueda(texto,
				resultado.size(), demora / 1000, new Instant().toDateTime(),
				this.nombreUsuario);

		this.notificar(log);

		return resultado;
	}

	public List<PuntoDeInteres> buscarPDI(String texto, String serv) {
		long demora = System.currentTimeMillis();

		List<PuntoDeInteres> resultado = repositorio.buscarPDI(texto, serv);

		demora -= System.currentTimeMillis();

		RegistroLogBusqueda log = new RegistroLogBusqueda(texto + "/" + serv,
				resultado.size(), demora / 1000, new Instant().toDateTime(),
				this.nombreUsuario);

		this.notificar(log);

		return resultado;

	}

	public boolean agregarObservador(ObservadorBusqueda obs) {
		// chequeo que sea siempre un observador diferente
		boolean repetido = this.observadores.stream().anyMatch(
				o -> obs.getClass().equals(o.getClass()));

		if (repetido)
			return false;

		observadores.add(obs);

		return true;
	}

	public void quitarObservador(ObservadorBusqueda obs) {
		observadores.remove(obs);
	}

	public void quitarObservador(Class<?> tipo) {
		observadores.removeIf(o -> o.getClass().equals(tipo));
	}

	public List<ObservadorBusqueda> getObservadores() {
		return observadores;
	}

	public void setObservadores(List<ObservadorBusqueda> obs) {
		observadores = obs;
	}

	protected void notificar(RegistroLogBusqueda log) {
		observadores.forEach(obs -> obs.actualizar(log));
	}

	public void setRepositorio(BuscadorPDIs repo) {
		repositorio = repo;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}
}
