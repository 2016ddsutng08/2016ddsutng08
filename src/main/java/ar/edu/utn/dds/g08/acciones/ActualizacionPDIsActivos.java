package ar.edu.utn.dds.g08.acciones;

import java.util.*;

public interface ActualizacionPDIsActivos {

	public List<BajaPDI> pdisInactivos();

}
