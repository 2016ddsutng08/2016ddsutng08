package ar.edu.utn.dds.g08.datos;

import java.util.Arrays;

import ar.edu.utn.dds.g08.dominio.CGP;
import ar.edu.utn.dds.g08.dominio.Comuna;
import ar.edu.utn.dds.g08.dominio.Direccion;
import ar.edu.utn.dds.g08.dominio.Horario;
import ar.edu.utn.dds.g08.dominio.Servicio;
import ar.edu.utn.dds.g08.valueClasses.*;

public class CreadorCGP {
	PointAdaptado punto01 = new PointAdaptado(-34.659074, -58.469602);
	PointAdaptado punto02 = new PointAdaptado(-34.657548, -58.467666);
	PointAdaptado punto03 = new PointAdaptado(-34.660019, -58.464501);
	PointAdaptado punto04 = new PointAdaptado(-34.663822, -58.469265);
	PointAdaptado punto05 = new PointAdaptado(-34.660535, -58.467513);
	PointAdaptado punto06 = new PointAdaptado(-34.661535, -58.477513);
	PointAdaptado punto07 = new PointAdaptado(-34.662535, -58.487513);
	PointAdaptado punto08 = new PointAdaptado(-34.663535, -58.497513);

	Comuna comuna01 = new Comuna("Comuna 01", new PolygonAdaptado(
			Arrays.asList(punto01, punto02, punto05)));
	Comuna comuna02 = new Comuna("Comuna 02", new PolygonAdaptado(
			Arrays.asList(punto02, punto03, punto05)));
	Comuna comuna03 = new Comuna("Comuna 03", new PolygonAdaptado(
			Arrays.asList(punto03, punto04, punto05)));

	Comuna comuna04 = new Comuna("Comuna 04", new PolygonAdaptado(
			Arrays.asList(punto06, punto07, punto08)));

	Horario horario = new Horario(1, 5, 9, 0, 18, 0); // Lunes a viernes
														// de 9 a
	// 18

	Servicio servicio = new Servicio("atención ciudadana",
			Arrays.asList(horario));
	Servicio servEspecial = new Servicio("Kiosko", Arrays.asList(new Horario(7,
			7, 9, 0, 18, 0))); // Domingo

	Servicio servicioQ = new Servicio("Quejas", Arrays.asList(horario));

	public CGP buildCGP1() {
		return new CGP(
				new Direccion(new PointAdaptado(-34.659079, -58.468545)),
				comuna01, Arrays.asList(servicio, servEspecial));
	}

	public CGP buildCGP2() {
		return new CGP(
				new Direccion(new PointAdaptado(-34.658364, -58.468245)),
				comuna01, Arrays.asList(servicio));
	}

	public CGP buildCGP3() {
		return new CGP(
				new Direccion(new PointAdaptado(-34.659873, -58.468084)),
				comuna01, Arrays.asList(servicio));
	}

	public CGP buildCGP4() {
		return new CGP(
				new Direccion(new PointAdaptado(-34.658549, -58.467118)),
				comuna02, Arrays.asList(servicio));
	}

	public CGP buildCGP5() {
		return new CGP(
				new Direccion(new PointAdaptado(-34.660411, -58.466989)),
				comuna02, Arrays.asList(servicio));
	}

	public CGP buildCGP6() {
		return new CGP(
				new Direccion(new PointAdaptado(-34.659802, -58.465541)),
				comuna02, Arrays.asList(servicio));
	}

	public CGP buildCGP7() {
		return new CGP(
				new Direccion(new PointAdaptado(-34.658919, -58.466217)),
				comuna02, Arrays.asList(servicio));
	}

	public CGP buildCGP8() {
		return new CGP(
				new Direccion(new PointAdaptado(-34.662518, -58.467803)),
				comuna03, Arrays.asList(servicio));
	}

	public CGP buildCGP9() {
		return new CGP(
				new Direccion(new PointAdaptado(-34.661018, -58.466977)),
				comuna03, Arrays.asList(servicio));
	}

	public CGP buildCGP10() {
		return new CGP(
				new Direccion(new PointAdaptado(-34.661194, -58.466140)),
				comuna03, Arrays.asList(servicio));
	}

}
