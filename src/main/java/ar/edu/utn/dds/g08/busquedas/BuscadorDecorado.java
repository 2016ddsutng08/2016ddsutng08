package ar.edu.utn.dds.g08.busquedas;

import java.util.List;

import ar.edu.utn.dds.g08.dominio.PuntoDeInteres;

public abstract class BuscadorDecorado implements BuscadorPDIs {

	protected BuscadorPDIs decorado;

	public BuscadorDecorado(BuscadorPDIs d) {
		decorado = d;
	}

	@Override
	public PuntoDeInteres buscarPDI(Integer key) {
		return decorado.buscarPDI(key);
	}

	@Override
	public PuntoDeInteres buscarPDIPorId(int key) {
		return decorado.buscarPDIPorId(key);
	}

	@Override
	public List<PuntoDeInteres> buscarPDI(String texto) {
		return decorado.buscarPDI(texto);
	}

	@Override
	public List<PuntoDeInteres> buscarPDI(String texto, String serv) {
		return decorado.buscarPDI(texto, serv);
	}

}
