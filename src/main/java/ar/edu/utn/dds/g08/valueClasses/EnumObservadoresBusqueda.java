package ar.edu.utn.dds.g08.valueClasses;

public enum EnumObservadoresBusqueda {
	HISTORIAL("Historial"), LOG("Log de búsquedas"), NOTIFICAR_DEMORA(
			"Notificar Demora");

	private final String texto;

	private EnumObservadoresBusqueda(final String t) {
		this.texto = t;
	}

	@Override
	public String toString() {
		return texto;
	}

}
