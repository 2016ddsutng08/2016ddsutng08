package ar.edu.utn.dds.g08.datos;

import java.util.Arrays;

import ar.edu.utn.dds.g08.dominio.Direccion;
import ar.edu.utn.dds.g08.dominio.Servicio;
import ar.edu.utn.dds.g08.dominio.SucursalBanco;
import ar.edu.utn.dds.g08.valueClasses.*;

public class CreadorSucursalBanco {
	Servicio caja = new Servicio("Caja",
			Arrays.asList(SucursalBanco.HORARIO_BANCARIO));
	Servicio inversiones = new Servicio("Inversiones",
			Arrays.asList(SucursalBanco.HORARIO_BANCARIO));

	public SucursalBanco buildBanco1() {
		return new SucursalBanco(new Direccion(
				new PointAdaptado(-34.636648, -58.364657)), Arrays.asList(caja),
				"Galicia equipos", "Boca");
	}

	public SucursalBanco buildBanco2() {
		return new SucursalBanco(new Direccion(
				new PointAdaptado(-34.546465, -58.450952)), Arrays.asList(caja,
				inversiones), "Galicia equipos", "Riber");
	}

	public SucursalBanco buildBanco3() {
		return new SucursalBanco(new Direccion(
				new PointAdaptado(-34.510001, -58.506366)), Arrays.asList(caja,
				inversiones), "Galicia cheto", "Chetos");
	}

	public SucursalBanco buildBanco4() {
		return new SucursalBanco(new Direccion(
				new PointAdaptado(-34.669773, -58.370346)), Arrays.asList(caja,
				inversiones), "Francés", "Frio");
	}

	public SucursalBanco buildBanco5() {
		return new SucursalBanco(new Direccion(
				new PointAdaptado(-34.659842, -58.468496)), Arrays.asList(caja,
				inversiones), "Francés", "UTN");
	}

	public SucursalBanco buildBanco6() {
		return new SucursalBanco(new Direccion(
				new PointAdaptado(-34.591490, -58.434517)), Arrays.asList(caja,
				inversiones), "Francés", "Fede");
	}

	public SucursalBanco buildBanco7() {
		return new SucursalBanco(new Direccion(
				new PointAdaptado(-34.449732, -58.662604)), Arrays.asList(caja),
				"ICBC", "Frigo");
	}

	public SucursalBanco buildBanco8() {
		return new SucursalBanco(new Direccion(
				new PointAdaptado(-34.542826, -58.711773)), Arrays.asList(caja),
				"ICBC", "Casa");
	}

	public SucursalBanco buildBanco9() {
		return new SucursalBanco(new Direccion(
				new PointAdaptado(-34.610236, -58.908390)), Arrays.asList(caja,
				inversiones), "Francés Go", "Lejos");
	}

	public SucursalBanco buildBanco10() {
		return new SucursalBanco(new Direccion(
				new PointAdaptado(-34.513290, -58.736901)), Arrays.asList(caja,
				inversiones), "Francés Go", "EAA");
	}

}
