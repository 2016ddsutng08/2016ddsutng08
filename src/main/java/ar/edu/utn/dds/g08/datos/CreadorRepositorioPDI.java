package ar.edu.utn.dds.g08.datos;

import ar.edu.utn.dds.g08.fuentesDatos.RepositorioPDI;

public class CreadorRepositorioPDI {
	public RepositorioPDI buildRepositorio() {
		RepositorioPDI repositorio = new RepositorioPDI();

		CreadorCGP builderCGP = new CreadorCGP();
		CreadorColectivo builderColectivo = new CreadorColectivo();
		CreadorLocalComercial builderLocalComercial = new CreadorLocalComercial();
		CreadorSucursalBanco builderSucursalBanco = new CreadorSucursalBanco();

		repositorio.putPDI(builderCGP.buildCGP1());
		repositorio.putPDI(builderCGP.buildCGP4());
		repositorio.putPDI(builderCGP.buildCGP8());

		repositorio.putPDI(builderColectivo.buildColectivo1());
		repositorio.putPDI(builderColectivo.buildColectivo2());
		repositorio.putPDI(builderColectivo.buildColectivo3());
		repositorio.putPDI(builderColectivo.buildColectivo4());
		repositorio.putPDI(builderColectivo.buildColectivo5());
		repositorio.putPDI(builderColectivo.buildColectivo6());
		repositorio.putPDI(builderColectivo.buildColectivo7());
		repositorio.putPDI(builderColectivo.buildColectivo8());
		repositorio.putPDI(builderColectivo.buildColectivo9());
		repositorio.putPDI(builderColectivo.buildColectivo10());

		repositorio.putPDI(builderColectivo.paradaBoedo);
		repositorio.putPDI(builderColectivo.paradaCampus);
		repositorio.putPDI(builderColectivo.paradaConstitucion);
		repositorio.putPDI(builderColectivo.paradaDevoto);
		repositorio.putPDI(builderColectivo.paradaMedrano);
		repositorio.putPDI(builderColectivo.paradaPalermo);
		repositorio.putPDI(builderColectivo.paradaPteSaavedra);
		repositorio.putPDI(builderColectivo.paradaRetiro);

		repositorio.putPDI(builderLocalComercial.buildLocal1());
		repositorio.putPDI(builderLocalComercial.buildLocal2());
		repositorio.putPDI(builderLocalComercial.buildLocal3());
		repositorio.putPDI(builderLocalComercial.buildLocal4());
		repositorio.putPDI(builderLocalComercial.buildLocal5());
		repositorio.putPDI(builderLocalComercial.buildLocal6());
		repositorio.putPDI(builderLocalComercial.buildLocal7());
		repositorio.putPDI(builderLocalComercial.buildLocal8());
		repositorio.putPDI(builderLocalComercial.buildLocal9());
		repositorio.putPDI(builderLocalComercial.buildLocal10());

		repositorio.putPDI(builderSucursalBanco.buildBanco1());
		repositorio.putPDI(builderSucursalBanco.buildBanco2());
		repositorio.putPDI(builderSucursalBanco.buildBanco3());
		repositorio.putPDI(builderSucursalBanco.buildBanco4());
		repositorio.putPDI(builderSucursalBanco.buildBanco5());
		repositorio.putPDI(builderSucursalBanco.buildBanco6());
		repositorio.putPDI(builderSucursalBanco.buildBanco7());
		repositorio.putPDI(builderSucursalBanco.buildBanco8());
		repositorio.putPDI(builderSucursalBanco.buildBanco9());
		repositorio.putPDI(builderSucursalBanco.buildBanco10());

		return repositorio;
	}
}
