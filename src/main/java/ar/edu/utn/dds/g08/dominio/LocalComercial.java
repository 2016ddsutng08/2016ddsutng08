package ar.edu.utn.dds.g08.dominio;

import java.util.Arrays;
import java.util.List;

import org.joda.time.*;

import ar.edu.utn.dds.g08.excepciones.NullParametro;
import ar.edu.utn.dds.g08.valueClasses.*;

import javax.persistence.*;

@Entity
public class LocalComercial extends PuntoDeInteres {

	@ManyToOne(cascade = CascadeType.ALL)
	protected Rubro rubro;

	@ElementCollection
	protected List<String> palabrasClaves;

	@ElementCollection
	protected List<Horario> horarios;

	protected String nombreFantasia;

	public LocalComercial() {

	}

	public LocalComercial(String icono, Direccion direccion, Rubro r,
			List<String> pc, List<Horario> h, String n) {
		super(icono, direccion);

		if (pc == null || n == null)
			throw new NullParametro("Ingrese valores no nulos");

		if (r == null)
			this.setRubro(new Rubro("", 0));
		else
			this.setRubro(r);

		this.setPalabrasClaves(pc);

		if (h == null)
			this.setHorarios(Arrays.asList());
		else
			this.setHorarios(h);

		this.setNombre(n);
	}

	public Rubro getRubro() {
		return rubro;
	}

	public void setRubro(Rubro rubro) {
		this.rubro = rubro;
	}

	protected void setNombre(String nombre) {
		this.nombreFantasia = nombre;
	}

	public List<String> getPalabrasClaves() {
		return palabrasClaves;
	}

	public void setPalabrasClaves(List<String> palabrasClave) {
		this.palabrasClaves = palabrasClave;
	}

	public void addPalabraClave(String palabraClave) {
		this.palabrasClaves.add(palabraClave);
	}

	public List<Horario> getHorarios() {
		return horarios;
	}

	public void setHorarios(List<Horario> horarios) {
		this.horarios = horarios;
	}

	@Override
	public boolean esCercano(PointAdaptado unPunto) {
		return this.direccion.distancia(unPunto) < this.rubro.getRadio();
	}

	@Override
	public boolean estaDisponible(Instant instante) {
		return this.horarios.stream().anyMatch(
				horario -> horario.estaDisponible(instante));
	}

	@Override
	public boolean estaDisponible(Instant instante, String servicio) {
		return this.estaDisponible(instante);
	}

	@Override
	public boolean estaEnTexto(String texto) {
		return (this.nombreFantasia.startsWith(texto)
				|| this.palabrasClaves.contains(texto) || this.rubro
				.getNombre().equals(texto));
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof LocalComercial))
			return false;

		LocalComercial local = (LocalComercial) obj;

		// son iguales cuando tienen = nombre y rubro
		return (this.nombreFantasia == local.getNombreFantasia())
				&& this.rubro.equals(local.getRubro());
	}

	@Override
	public int hashCode() {
		return this.nombreFantasia.hashCode() + this.rubro.hashCode();
	}

	public String getNombreFantasia() {
		return this.nombreFantasia;
	}

	@Override
	public String toString() {
		return "Local" + " " + this.nombreFantasia;
	}
}