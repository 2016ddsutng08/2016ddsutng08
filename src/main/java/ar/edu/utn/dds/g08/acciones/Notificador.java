package ar.edu.utn.dds.g08.acciones;

import ar.edu.utn.dds.g08.valueClasses.RegistroEstadoProceso;

public interface Notificador {

	void notificar(EstadoProceso est, RegistroEstadoProceso reg);
}
