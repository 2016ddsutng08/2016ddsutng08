package ar.edu.utn.dds.g08.acciones;

import java.util.*;

public class BajaPDI {
	String valorBusqueda;
	Date fechaBaja;

	public BajaPDI(String v, Date f) {
		valorBusqueda = v;
		fechaBaja = f;
	}

	public String getValorBusqueda() {
		return valorBusqueda;
	}

	public Date getFechaBaja() {
		return fechaBaja;
	}
}
