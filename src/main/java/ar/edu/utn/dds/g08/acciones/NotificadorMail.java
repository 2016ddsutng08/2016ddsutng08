package ar.edu.utn.dds.g08.acciones;

import ar.edu.utn.dds.g08.perfilesUsuarios.*;
import ar.edu.utn.dds.g08.valueClasses.RegistroEstadoProceso;

public class NotificadorMail implements Notificador {

	PerfilAdministrador usuario;

	@Override
	public void notificar(EstadoProceso est, RegistroEstadoProceso reg) {
		System.out.println("Se notifica del resultado fallido a "
				+ usuario.getMail());

		reg.setFinalizadoError("Error en procesar Acción");
	}

	public void setUsuario(PerfilAdministrador adm) {
		usuario = adm;
	}
}
