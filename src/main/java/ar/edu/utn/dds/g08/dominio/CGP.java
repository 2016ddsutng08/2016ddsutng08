package ar.edu.utn.dds.g08.dominio;

import java.util.List;

import org.joda.time.Instant;

import ar.edu.utn.dds.g08.excepciones.NullParametro;
import ar.edu.utn.dds.g08.valueClasses.*;

import javax.persistence.*;

@Entity
public class CGP extends PuntoDeInteres {

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	private Comuna comuna; // cada comuna tiene SOLO 1 CGP

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
	protected List<Servicio> servicios;

	private String director;
	private String telefono;

	public CGP() {

	}

	public CGP(Direccion direccion, Comuna c, List<Servicio> s) {
		super("icono CGP", direccion);

		if (c == null || s == null)
			throw new NullParametro(
					"Ingrese valores no nulos para Comuna y Serv");

		this.comuna = c;
		this.setServicios(s);
	}

	public Comuna getComuna() {
		return comuna;
	}

	public List<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(List<Servicio> servicios) {
		this.servicios = servicios;
	}

	@Override
	public boolean esCercano(PointAdaptado unPunto) {
		return super.esCercano(unPunto) || this.comuna.esCercano(unPunto);
	}

	@Override
	public boolean estaDisponible(Instant instante) {
		return this.servicios.stream().anyMatch(
				servicio -> servicio.estaDisponible(instante));
	}

	@Override
	public boolean estaDisponible(Instant instante, String servicio) {
		Servicio servicioSel = this.getServicio(servicio);

		if (servicioSel == null)
			return false;
		else
			return servicioSel.estaDisponible(instante);
	}

	public Servicio getServicio(String nombre) {
		Servicio sel = null;

		for (Servicio servicio : this.servicios) {
			if (servicio.getNombre() == nombre)
				sel = servicio;
		}

		return sel;
	}

	@Override
	public boolean estaEnTexto(String texto) {
		return (comuna.getNumero().equals(texto) || this
				.contieneServicio(texto));
	}

	public boolean contieneServicio(String texto) {
		return this.getServicios().stream()
				.anyMatch(servicio -> servicio.getNombre().startsWith(texto));
	}

	public String getDirector() {
		return this.director;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setDirector(String dir) {
		this.director = dir;
	}

	public void setTelefono(String tel) {
		this.telefono = tel;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof CGP))
			return false;

		CGP cgp = (CGP) obj;

		return this.comuna.equals(cgp.getComuna()); // ya q la relacion es 1 a
													// 1, podria agregar luego
													// la direccion.
	}

	@Override
	public int hashCode() {
		return this.comuna.hashCode();
	}
	
	@Override
	public String toString(){
		return "CGP" + " " + this.comuna.toString(); 
	}
}