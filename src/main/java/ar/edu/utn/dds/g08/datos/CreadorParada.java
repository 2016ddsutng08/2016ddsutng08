package ar.edu.utn.dds.g08.datos;

import ar.edu.utn.dds.g08.dominio.Direccion;
import ar.edu.utn.dds.g08.dominio.Parada;
import ar.edu.utn.dds.g08.valueClasses.*;

public class CreadorParada extends CreadorRepositorioPDI {
	public Parada buildParada1() {
		return new Parada("icono parada", new Direccion(new PointAdaptado(-34.659932,
				-58.468362)));
	}

	public Parada buildParada2() {
		return new Parada("icono parada", new Direccion(new PointAdaptado(-34.591678,
				-58.374675)));
	}

	public Parada buildParada3() {
		return new Parada("icono parada", new Direccion(new PointAdaptado(-34.627820,
				-58.380990)));
	}

	public Parada buildParada4() {
		return new Parada("icono parada", new Direccion(new PointAdaptado(-34.625111,
				-58.416162)));
	}

	public Parada buildParada5() {
		return new Parada("icono parada", new Direccion(new PointAdaptado(-34.600290,
				-58.511885)));
	}

	public Parada buildParada6() {
		return new Parada("icono parada", new Direccion(new PointAdaptado(-34.578355,
				-58.425631)));
	}

	public Parada buildParada7() {
		return new Parada("icono parada", new Direccion(new PointAdaptado(-34.537468,
				-58.476507)));
	}

	public Parada buildParada8() {
		return new Parada("icono parada", new Direccion(new PointAdaptado(-34.598552,
				-58.420274)));
	}

}
