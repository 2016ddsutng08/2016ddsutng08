package ar.edu.utn.dds.g08.valueClasses;

public enum EnumAccionesUsuario {
	BUSCAR_PDIS("pdis"), ACCIONES_BUSQUEDAS("acciones"), REPORTES(
			"historial");

	private final String texto;

	private EnumAccionesUsuario(final String t) {
		this.texto = t;
	}

	@Override
	public String toString() {
		return texto;
	}

}
