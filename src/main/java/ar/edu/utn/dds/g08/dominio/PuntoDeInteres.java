package ar.edu.utn.dds.g08.dominio;

import org.joda.time.*;

import javax.persistence.*;

import ar.edu.utn.dds.g08.valueClasses.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class PuntoDeInteres {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected Integer id;

	protected String icono; // falta revisar de qué tipo será

	@Embedded
	protected Direccion direccion; // Incluye la ubicación

	protected PuntoDeInteres() {

	}

	public PuntoDeInteres(String icono, Direccion direccion) {
		this();

		/*
		 * if(direccion == null) throw new
		 * NullParametro("Ingrese valores no nulos para dirección");
		 */

		this.icono = icono;
		this.setDireccion(direccion);
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public Direccion getDireccion() {
		return this.direccion;
	}

	public boolean esCercano(PointAdaptado ubicacion) {
		return this.direccion.esCercano(ubicacion);
	}

	public boolean estaDisponible(Instant instante) {
		return true; // ante la duda siempre responde true
	}

	public boolean estaDisponible(Instant instante, String servicio) {
		return true; // ante la duda siempre responde true
	}

	public boolean estaEnTexto(String texto) {
		return true;
	}

	public void setId(Integer key) {
		id = key;
	}

	public Integer getId() {
		return id;
	}

}
