package ar.edu.utn.dds.g08.busquedas;

import ar.edu.utn.dds.g08.dominio.*;
import ar.edu.utn.dds.g08.fuentesDatos.*;

import java.util.*;
import java.util.stream.Collectors;

public class BuscadorCGPs implements BuscadorPDIs { // Es un ADAPTER
	
	ConsultaCGPs adaptable;

	public BuscadorCGPs(ConsultaCGPs repo) {
		adaptable = repo;
	}

	@Override
	public PuntoDeInteres buscarPDI(Integer key) {
		return null; // no hay KEY
	}

	@Override
	public PuntoDeInteres buscarPDIPorId(int key) {
		return null;
	}
	
	@Override
	public List<PuntoDeInteres> buscarPDI(String texto) {
		List<CentroDTO> cgps;

		cgps = adaptable.buscarCGPs(texto);

		return cgps.stream().map(c -> this.adaptarPDI(c))
				.collect(Collectors.toList());
	}

	private CGP adaptarPDI(CentroDTO centro) {
		Direccion dir = new Direccion(null, null, null, null,
				centro.getDomicilio(), 0, 0, null, 0, null, null);
		
		Comuna com = new Comuna(centro.getNroComuna().toString(), null);
		com.setZonasIncluidas(centro.getZonas());
		
		List<Servicio> serv = centro.getServicios().stream()
				.map(s -> this.adaptarServicio(s)).collect(Collectors.toList());

		CGP nuevo = new CGP(dir, com, serv);
		nuevo.setDirector(centro.getDirector());
		nuevo.setTelefono(centro.getTelefono());

		return nuevo;
	}

	private Servicio adaptarServicio(ServicioDTO servicioDTO) {
		List<Horario> horarios = servicioDTO.getRangos().stream()
				.map(r -> this.adaptarHorario(r)).collect(Collectors.toList());

		return new Servicio(servicioDTO.getNombre(), horarios);
	}

	private Horario adaptarHorario(RangoServicioDTO rango) {
		return new Horario(rango.getNumeroDia(), rango.getNumeroDia(),
				rango.getHorarioDesde(), rango.getMinutosDesde(),
				rango.getHorarioHasta(), rango.getMinutosHasta());
	}

	@Override
	public List<PuntoDeInteres> buscarPDI(String texto1,String texto2) {
		return buscarPDI(texto1);
	}
}
