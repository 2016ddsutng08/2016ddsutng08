package ar.edu.utn.dds.g08.acciones;

import ar.edu.utn.dds.g08.excepciones.AccionException;
import ar.edu.utn.dds.g08.fuentesDatos.RepositorioPDI;
import ar.edu.utn.dds.g08.dominio.*;

import java.util.*;

public class BajaPDIsInactivos extends Accion {

	ActualizacionPDIsActivos gobCiudad;
	RepositorioPDI repositorio;
	List<PuntoDeInteres> pdis;

	List<PuntoDeInteres> pdisEliminados;

	public BajaPDIsInactivos(ActualizacionPDIsActivos g, RepositorioPDI r) {
		gobCiudad = g;
		repositorio = r;
	}

	@Override
	public void ejecutarProcesoPropio() {
		List<BajaPDI> pdisInactivos = gobCiudad.pdisInactivos();

		pdis = repositorio.getAll();
		pdisEliminados = new ArrayList<PuntoDeInteres>();

		pdisInactivos.forEach(pinac -> eliminarPDI(pinac));

	}

	@Override
	public void deshacerProcesoPropio() throws Exception {
		throw new AccionException("no se puede deshacer");
	}

	public void eliminarPDI(BajaPDI pdiInactivo) {
		pdis.stream()
				.filter(p -> p.estaEnTexto(pdiInactivo.getValorBusqueda()))
				.forEach(p -> {
					repositorio.removePDI(p);
					pdisEliminados.add(p);
				});
	}

	public List<PuntoDeInteres> getPDIsEliminados() {
		return pdisEliminados;
	}
}
