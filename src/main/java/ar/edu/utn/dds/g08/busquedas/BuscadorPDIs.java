package ar.edu.utn.dds.g08.busquedas;

import java.util.List;
import ar.edu.utn.dds.g08.dominio.PuntoDeInteres;

public interface BuscadorPDIs {
	
	public PuntoDeInteres buscarPDI(Integer key);
	public PuntoDeInteres buscarPDIPorId(int key);
	public List<PuntoDeInteres> buscarPDI(String texto);
	public List<PuntoDeInteres> buscarPDI(String texto,String serv);
	
}
