package ar.edu.utn.dds.g08.acciones;

import ar.edu.utn.dds.g08.perfilesUsuarios.*;
import ar.edu.utn.dds.g08.valueClasses.*;
import java.util.*;

public class EstadoProceso {
	PerfilAdministrador usuario;
	Set<RegistroEstadoProceso> procesos;
	int cantidadAcciones = 0;

	public EstadoProceso(PerfilAdministrador usr) {
		usuario = usr;
		procesos = new HashSet<RegistroEstadoProceso>();
	}

	public void agregarProceso(RegistroEstadoProceso est) {
		synchronized (this) {
			procesos.add(est);
		}
	}

	public PerfilAdministrador getUsuario() {
		return usuario;
	}

	public void setCantidadAcciones(int c) {
		synchronized (this) {
			cantidadAcciones = c;
		}
	}

	public boolean estaFinalizado() {
		synchronized (this) {
			if (cantidadAcciones == procesos.size())
				return procesos.stream().allMatch(p -> p.estaFinalizado());
			else
				return false;
		}
	}

	public boolean finalizadoCorrectamente() {
		if (this.estaFinalizado())
			return procesos.stream().allMatch(p -> p.finalizadoCorrectamente());
		else
			return false;
	}

	public int getCantidadAcciones() {
		return cantidadAcciones;
	}
}
