package ar.edu.utn.dds.g08.acciones;

import java.util.Set;

import org.mockito.internal.util.collections.Sets;

public class AccionMultiple extends Accion {

	Set<Accion> acciones = Sets.newSet();

	@Override
	public void ejecutar(EstadoProceso estado) {
		estado.setCantidadAcciones(acciones.size());

		acciones.forEach(acc -> acc.ejecutar(estado));
	}

	@Override
	public void deshacer(EstadoProceso estado) {
		estado.setCantidadAcciones(acciones.size());

		acciones.forEach(acc -> acc.deshacer(estado));
	}

	public void addAccion(Accion acc) {
		acciones.add(acc);
	}
}
