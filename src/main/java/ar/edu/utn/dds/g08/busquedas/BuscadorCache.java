package ar.edu.utn.dds.g08.busquedas;

import java.util.HashMap;
import java.util.List;
import ar.edu.utn.dds.g08.dominio.*;

public class BuscadorCache extends BuscadorDecorado {

	private HashMap<String, List<PuntoDeInteres>> busquedasRealizadas;

	public BuscadorCache(BuscadorPDIs b) {
		super(b);

		busquedasRealizadas = new HashMap<String, List<PuntoDeInteres>>();
	}

	@Override
	public List<PuntoDeInteres> buscarPDI(String texto) {
		if (!busquedasRealizadas.containsKey(texto))
			busquedasRealizadas.put(texto, decorado.buscarPDI(texto));

		return busquedasRealizadas.get(texto);
	}

	@Override
	public List<PuntoDeInteres> buscarPDI(String texto, String serv) {
		if (!busquedasRealizadas.containsKey(texto + serv))
			busquedasRealizadas.put(texto + serv,
					decorado.buscarPDI(texto, serv));

		return busquedasRealizadas.get(texto + serv);
	}

}
