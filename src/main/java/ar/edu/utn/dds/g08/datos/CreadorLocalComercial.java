package ar.edu.utn.dds.g08.datos;

import java.util.Arrays;

import ar.edu.utn.dds.g08.dominio.Direccion;
import ar.edu.utn.dds.g08.dominio.Horario;
import ar.edu.utn.dds.g08.dominio.LocalComercial;
import ar.edu.utn.dds.g08.dominio.Rubro;
import ar.edu.utn.dds.g08.valueClasses.*;

public class CreadorLocalComercial {
	Rubro jugueterias = new Rubro("Juguetería", 1);
	Rubro comercioAlim = new Rubro("Alimentos", 0.1);
	Rubro indumentaria = new Rubro("Indumentaria", 5);

	Horario findes = new Horario(6, 7, 10, 00, 19, 00);
	Horario entreSem = new Horario(1, 5, 10, 00, 19, 00);
	Horario viernes = new Horario(5, 5, 10, 00, 19, 00);

	public LocalComercial buildLocal1() {
		return new LocalComercial("icono", new Direccion(new PointAdaptado(-34.603708,
				-58.381596)), jugueterias, Arrays.asList("Buzz", "Quija"),
				Arrays.asList(findes, viernes), "Carrousell");
	}

	public LocalComercial buildLocal2() {
		return new LocalComercial("icono", new Direccion(new PointAdaptado(-34.600670,
				-58.389589)), jugueterias, Arrays.asList("Oca", "Buzz"),
				Arrays.asList(findes), "Dalesandro");
	}

	public LocalComercial buildLocal3() {
		return new LocalComercial("icono", new Direccion(new PointAdaptado(-34.518606,
				-58.738618)), comercioAlim, Arrays.asList("Bondiola", "Asado"),
				Arrays.asList(entreSem), "Maxi");
	}

	public LocalComercial buildLocal4() {
		return new LocalComercial("icono", new Direccion(new PointAdaptado(-34.518056,
				-58.739448)), comercioAlim, Arrays.asList("Bondiola", "Asado"),
				Arrays.asList(entreSem, findes), "Rioplatense");
	}

	public LocalComercial buildLocal5() {
		return new LocalComercial("icono", new Direccion(new PointAdaptado(-34.514562,
				-58.742641)), comercioAlim, Arrays.asList("Bondiola", "Pollo",
				"Vacío"), Arrays.asList(entreSem, findes), "Rey");
	}

	public LocalComercial buildLocal6() {
		return new LocalComercial("icono", new Direccion(new PointAdaptado(-34.531739,
				-58.702492)), comercioAlim, Arrays.asList("Pollo", "leche",
				"Pan"), Arrays.asList(entreSem), "Coto");
	}

	public LocalComercial buildLocal7() {
		return new LocalComercial("icono", new Direccion(new PointAdaptado(-34.535464,
				-58.704859)), comercioAlim, Arrays.asList("Leche", "Pan",
				"Lavandina"), Arrays.asList(findes, viernes), "Chino");
	}

	public LocalComercial buildLocal8() {
		return new LocalComercial("icono", new Direccion(new PointAdaptado(-34.540439,
				-58.708562)), comercioAlim, Arrays.asList("Cerveza",
				"Servilleta", "Ajo"), Arrays.asList(entreSem, findes), "Jumbo");
	}

	public LocalComercial buildLocal9() {
		return new LocalComercial("icono", new Direccion(new PointAdaptado(-34.546163,
				-58.706680)), comercioAlim, Arrays.asList("Pan", "Coca"),
				Arrays.asList(entreSem, findes), "Vea");
	}

	public LocalComercial buildLocal10() {
		return new LocalComercial("icono", new Direccion(new PointAdaptado(-34.589894,
				-58.430139)), indumentaria, Arrays.asList("Jean", "Camisa"),
				Arrays.asList(findes, viernes), "Herencia");
	}

}
