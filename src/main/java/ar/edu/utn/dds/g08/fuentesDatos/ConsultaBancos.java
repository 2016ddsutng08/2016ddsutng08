package ar.edu.utn.dds.g08.fuentesDatos;

import org.json.JSONArray;

public interface ConsultaBancos {

	public JSONArray buscarBancos(String nombre, String servicio);
}
