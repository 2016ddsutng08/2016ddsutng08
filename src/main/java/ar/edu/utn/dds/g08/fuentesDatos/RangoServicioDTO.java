package ar.edu.utn.dds.g08.fuentesDatos;

public class RangoServicioDTO {
	private int numeroDia;
	private int horarioDesde;
	private int minutosDesde;
	private int horarioHasta;
	private int minutosHasta;

	public RangoServicioDTO(int dia,int horades,int horahas,int mindes,int minhas){
		numeroDia = dia;
		horarioDesde = horades;
		horarioHasta = horahas;
		minutosDesde = mindes;
		minutosHasta = minhas;
	}
	
	public void setMinutosHasta(int min) {
		this.minutosHasta = min;
	}
	
	public int getMinutosHasta() {
		return minutosHasta;
	}

	public void setHorarioHasta(int hora) {
		this.horarioHasta = hora;
	}

	public int getHorarioHasta() {
		return horarioHasta;
	}
	
	public void setMinutosDesde(int  min) {
		this.minutosDesde = min;
	}
	
	public int getMinutosDesde() {
		return minutosDesde;
	}

	public void setHorarioDesde(int hora) {
		this.horarioDesde = hora;
	}
	
	public int getHorarioDesde() {
		return horarioDesde;
	}

	public void setNumeroDia(int dia) {
		this.numeroDia = dia;
	}
	
	public int getNumeroDia() {
		return numeroDia;
	}
}
