package ar.edu.utn.dds.g08.fuentesDatos;

import java.util.List;

public class ServicioDTO {
	private String nombre;
	private List<RangoServicioDTO> rangos;

	public ServicioDTO(String nom,List<RangoServicioDTO> ran){
		nombre = nom;
		rangos =ran;
	}
	
	public void setNombre(String nom) {
		this.nombre = nom;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setRangos(List<RangoServicioDTO> ran) {
		this.rangos = ran;
	}
	
	public List<RangoServicioDTO> getRangos() {
		return rangos;
	}
}
