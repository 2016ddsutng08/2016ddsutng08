package ar.edu.utn.dds.g08.excepciones;

public class NullParametro extends RuntimeException{

	public NullParametro(String mensaje){
		super(mensaje);
	}
}
