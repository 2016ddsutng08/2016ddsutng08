package ar.edu.utn.dds.g08.persistencia;

import org.hibernate.SessionFactory;
import org.hibernate.boot.*;
import org.hibernate.boot.registry.*;

public class SessionFactorySingleton {

	private static SessionFactory instancia = null;

	public static SessionFactory getInstance() {
		if (instancia == null)
			crearInstancia();

		return instancia;
	}

	private static void crearInstancia() {
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
				.configure() // configura desde hibernate.cfg.xml
				.build();
		try {
			instancia = new MetadataSources(registry).buildMetadata()
					.buildSessionFactory();
		} catch (Exception e) {
			StandardServiceRegistryBuilder.destroy(registry);
			throw e;
		}
	}

}
