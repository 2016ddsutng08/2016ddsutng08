package ar.edu.utn.dds.g08.web;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.template.handlebars.HandlebarsTemplateEngine;
import static spark.Spark.*;

import java.util.*;

import org.hibernate.Session;

import ar.edu.utn.dds.g08.perfilesUsuarios.PerfilUsuario;
import ar.edu.utn.dds.g08.persistencia.SessionFactorySingleton;

public class LoginController extends AbstractControllerWeb {

	public static void init() {
		motorTemplates = new HandlebarsTemplateEngine();

		get("/login", (request, response) -> {
			return new ModelAndView(new HashMap<>(), "login.hbs");
		}, motorTemplates);

		post("/login", LoginController::login);

		get("/logout", LoginController::logout);
	}

	public static Object login(Request request, Response response) {
		PerfilUsuario usuario = null;

		String nombre = request.queryParams("usuario").toString();

		Session sesion = SessionFactorySingleton.getInstance().openSession();

		if (nombre != null)
			usuario = obtenerUsuario(sesion, nombre);

		if (usuario != null
				&& usuario.getContrasenia().equals(
						request.queryParams("password").toString())) {

			request.session().attribute("nombreUsuario",
					usuario.getNombreUsuario());

			MenuController.init();
			PDIController.init();
			AccionController.init();
			HistorialController.init();
			
			response.redirect("/menu");

		} else
			response.redirect("/login");

		sesion.close();

		return null;

	}

	public static Object logout(Request request, Response response) {
		request.session().removeAttribute("nombreUsuario");
		response.redirect("/login");
		return null;
	}
}
