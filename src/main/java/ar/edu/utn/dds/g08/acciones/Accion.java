package ar.edu.utn.dds.g08.acciones;

import ar.edu.utn.dds.g08.valueClasses.*;

public abstract class Accion {
	Notificador notificador;

	RegistroEstadoProceso estado;

	EstadoProceso controladorEstado;

	public void setNotificador(Notificador n) {
		notificador = n;
	}

	public void ejecutar(EstadoProceso controlador) {

		controladorEstado = controlador;

		estado = new RegistroEstadoProceso(getClass().getName(),
				controladorEstado.getUsuario().getNombreUsuario());

		controladorEstado.agregarProceso(estado);

		try {

			this.ejecutarProcesoPropio();

			estado.setFinalizadoOk();

		} catch (Exception exc) {
			notificador.notificar(controladorEstado, estado);
		}

	}

	public void deshacer(EstadoProceso controlador) {
		controladorEstado = controlador;

		estado = new RegistroEstadoProceso(getClass().getName(),
				controladorEstado.getUsuario().getNombreUsuario());

		controladorEstado.agregarProceso(estado);

		try {

			this.deshacerProcesoPropio();

			estado.setFinalizadoOk();

		} catch (Exception exc) {
			notificador.notificar(controladorEstado, estado);
		}

	}

	public void ejecutarProcesoPropio() throws Exception {

	}

	public void deshacerProcesoPropio() throws Exception {

	}

}
