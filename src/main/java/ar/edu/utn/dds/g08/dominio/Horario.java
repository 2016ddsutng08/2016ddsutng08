package ar.edu.utn.dds.g08.dominio;

import org.joda.time.*;

import ar.edu.utn.dds.g08.excepciones.NullParametro;

import javax.persistence.*;

import org.hibernate.annotations.Type;

@Embeddable
public class Horario {

	protected int diaInicio;
	protected int diaFin;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTimeAsString")
	protected LocalTime horaInicio;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTimeAsString")
	protected LocalTime horaFin;

	public Horario() {

	}

	public Horario(int diaIn, int diaF, int horaIni, int minIni, int horaF,
			int minF) {

		if (diaIn < 1 || diaIn > 7 || diaF < 1 || diaF > 7 || horaIni < 0
				|| horaIni > 23 || minIni < 0 || minIni > 59 || horaF < 0
				|| horaF > 23 || minF < 0 || minF > 59)
			throw new NullParametro("Ingrese valores compatibles con fechas");

		this.diaInicio = diaIn;
		this.diaFin = diaF;
		this.horaInicio = new LocalTime(horaIni, minIni);
		this.horaFin = new LocalTime(horaF, minF);
	}

	public int getDiaInicio() {
		return diaInicio;
	}

	public void setDiaInicio(int diaInicio) {
		this.diaInicio = diaInicio;
	}

	public int getDiaFin() {
		return diaFin;
	}

	public void setDiaFin(int diaFin) {
		this.diaFin = diaFin;
	}

	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(LocalTime horaInicio) {
		this.horaInicio = horaInicio;
	}

	public LocalTime getHoraFin() {
		return horaFin;
	}

	public void setHoraFin(LocalTime horaFin) {
		this.horaFin = horaFin;
	}

	public boolean estaDisponible(Instant fecha) {
		DateTime date = fecha.toDateTime();
		int dia = date.getDayOfWeek();
		int hora = date.getHourOfDay();
		int minuto = date.getMinuteOfHour();
		boolean disponible = (dia >= getDiaInicio()
				&& dia <= getDiaFin()
				&& (hora > getHoraInicio().getHourOfDay() || (hora == getHoraInicio()
						.getHourOfDay() && minuto >= getHoraInicio()
						.getMinuteOfHour())) && (hora < getHoraFin()
				.getHourOfDay() || (hora == getHoraFin().getHourOfDay() && minuto <= getHoraFin()
				.getMinuteOfHour())));
		return disponible;

	}

	@Override
	public String toString() {
		return "De " + this.getDiaString(diaInicio) + " A "
				+ this.getDiaString(diaFin) + " De " + horaInicio.toString()
				+ " A " + horaFin.toString();
	}

	public String getDiaString(int d) {
		switch (d) {
		case 1:
			return "Lunes";
		case 2:
			return "Martes";
		case 3:
			return "Miércoles";
		case 4:
			return "Jueves";
		case 5:
			return "Viernes";
		case 6:
			return "Sábado";
		case 7:
			return "Domingo";
		}

		return "";
	}
}
