package ar.edu.utn.dds.g08.acciones;

import ar.edu.utn.dds.g08.valueClasses.RegistroEstadoProceso;

public class NotificadorVacio implements Notificador {

	@Override
	public void notificar(EstadoProceso est, RegistroEstadoProceso reg) {
		reg.setFinalizadoError("Error en procesar Acción");
	}

}
