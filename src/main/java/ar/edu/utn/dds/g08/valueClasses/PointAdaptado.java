package ar.edu.utn.dds.g08.valueClasses;

import org.uqbar.geodds.*;

import javax.persistence.*;

@Entity
public class PointAdaptado {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "latitud", scale = 6, precision = 10)
	private double x = 0;

	@Column(name = "longitud", scale = 6, precision = 10)
	private double y = 0;

	@Transient
	private Point punto;

	protected PointAdaptado() {
		this.punto = new Point(x, y);
	}

	public PointAdaptado(double a, double b) {
		x = a;
		y = b;

		this.punto = new Point(x, y);
	}

	public void setX(double a) {
		x = a;

		this.punto = new Point(x, y);
	}

	public void setY(double a) {
		y = a;

		this.punto = new Point(x, y);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public Point getPoint() {
		return this.punto;
	}

	public double distance(PointAdaptado otroP) {
		return this.punto.distance(otroP.getPoint());
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PointAdaptado))
			return false;

		PointAdaptado point = (PointAdaptado) obj;

		return this.getX() == point.getX() && this.getY() == point.getY();

	}

	@Override
	public int hashCode() {
		return (int) (this.getX() + this.getY());
	}

	@Override
	public String toString() {
		return "Latitud" + " " + this.x + " ; " + "Longitud" + " " + this.y;
	}
}
