package ar.edu.utn.dds.g08.valueClasses;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Immutable;
import org.joda.time.*;

import javax.persistence.*;

@Entity
@Immutable
public class RegistroLogBusqueda {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected Integer id;

	protected String nombreUsuario;
	protected String fraseBuscada;
	protected int cantidadResultados;
	protected long miliSegDemora;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateAsString")
	protected LocalDate fechaBusqueda;

	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTimeAsString")
	protected LocalTime horaBusqueda;

	public RegistroLogBusqueda() {

	}

	public RegistroLogBusqueda(String frase, int cant, long miliseg,
			DateTime mom, String usu) {
		fraseBuscada = frase;
		cantidadResultados = cant;
		miliSegDemora = miliseg;
		fechaBusqueda = mom.toLocalDate();
		horaBusqueda = mom.toLocalTime();
		nombreUsuario = usu;
	}

	public String getFraseBuscada() {
		return fraseBuscada;
	}

	public int getCantidadResultados() {
		return cantidadResultados;
	}

	public long getMiliSegDemora() {
		return miliSegDemora;
	}

	public LocalDate getFechaBusqueda() {
		return fechaBusqueda;
	}

	public LocalTime getHoraBusqueda() {
		return horaBusqueda;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setFechaBusqueda(LocalDate f) {
		this.fechaBusqueda = f;
	}

	public void setId(int i) {
		this.id = i;
	}

	public int getId() {
		return this.id;
	}
}
