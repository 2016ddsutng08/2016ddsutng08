package ar.edu.utn.dds.g08.dominio;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ar.edu.utn.dds.g08.valueClasses.*;

import javax.persistence.*;

@Entity
public class Parada extends PuntoDeInteres {

	@OneToMany(mappedBy = "parada", cascade = CascadeType.ALL, orphanRemoval = false)
	public List<ParadaColectivo> lineas = new ArrayList<ParadaColectivo>();

	public Parada() {

	}

	public Parada(String icono, Direccion direccion) {
		super(icono, direccion);

		// las lineas en principio van vacías
	}

	public void setLineas(List<Colectivo> lineas) {
		lineas.forEach(l -> this.lineas.add(new ParadaColectivo(this, l)));
	}

	public List<Colectivo> getLineas() {
		return this.lineas.stream().map(pc -> pc.getColectivo())
				.collect(Collectors.toList());
	}

	@Override
	public boolean esCercano(PointAdaptado unPunto) {
		return this.direccion.distancia(unPunto) < 0.1;
	}

	@Override
	public boolean estaEnTexto(String texto) {
		try {
			return this.lineas.stream().map(pc -> pc.getColectivo())
					.anyMatch(linea -> linea.estaEnTexto(texto));
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Parada))
			return false;

		Parada parada = (Parada) obj;

		// son iguales cuando tienen = direccion
		return this.direccion.equals(parada.getDireccion());

	}

	@Override
	public int hashCode() {
		return this.direccion.hashCode();
	}

	@Override
	public String toString() {
		return "Parada Calle" + " " + this.direccion.toString();
	}
}
