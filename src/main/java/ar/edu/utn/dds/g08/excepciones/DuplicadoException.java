package ar.edu.utn.dds.g08.excepciones;

public class DuplicadoException extends RuntimeException{
		public DuplicadoException(){
			super("El PDI que quiere ingresar ya existe");
		}
}
