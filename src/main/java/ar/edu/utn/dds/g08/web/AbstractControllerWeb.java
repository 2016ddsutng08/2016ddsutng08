package ar.edu.utn.dds.g08.web;

import ar.edu.utn.dds.g08.busquedas.BuscadorPDIs;
import ar.edu.utn.dds.g08.busquedas.BuscadorRepositorio;
import ar.edu.utn.dds.g08.dominio.PuntoDeInteres;
import ar.edu.utn.dds.g08.fuentesDatos.RepositorioPDI;
import ar.edu.utn.dds.g08.perfilesUsuarios.PerfilUsuario;
import ar.edu.utn.dds.g08.valueClasses.*;

import org.hibernate.Session;
import org.hibernate.Transaction;

import spark.template.handlebars.HandlebarsTemplateEngine;

public abstract class AbstractControllerWeb {
	protected static HandlebarsTemplateEngine motorTemplates;

	public static PerfilUsuario obtenerUsuario(Session sesion, String nombre) {
		PerfilUsuario usuario = sesion.find(PerfilUsuario.class, nombre);

		return usuario;
	}

	public static BuscadorPDIs obtenerBuscadorPDIs(Session sesion) {
		RepositorioPDI repo = new RepositorioPDI();

		sesion.createQuery("from PuntoDeInteres", PuntoDeInteres.class)
				.getResultList().forEach(p -> repo.putPDI(p));

		BuscadorPDIs buscador = new BuscadorRepositorio(repo);

		return buscador;
	}

	public static GeneradorReportes obtenerGeneradorReportes(Session sesion) {
		GeneradorReportes reportes = new GeneradorReportes();

		sesion.createQuery("from RegistroLogBusqueda",
				RegistroLogBusqueda.class).getResultList()
				.forEach(l -> reportes.addBusqueda(l));

		return reportes;
	}

	public static void withTransaction(Session sesion, Runnable command) {
		Transaction transaccion = sesion.beginTransaction();

		try {
			command.run();
		} catch (Exception ex) {
			ex.printStackTrace();
			transaccion.rollback();
			sesion.close();
		}

		transaccion.commit();

	}

}
