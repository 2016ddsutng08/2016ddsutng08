package ar.edu.utn.dds.g08.excepciones;

public class InexistenteException extends RuntimeException {
	public InexistenteException(String msj) {
		super(msj);
	}
}
