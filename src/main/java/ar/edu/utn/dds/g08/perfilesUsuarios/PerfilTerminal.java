package ar.edu.utn.dds.g08.perfilesUsuarios;

import ar.edu.utn.dds.g08.busquedas.BuscadorPDIs;

import javax.persistence.*;

@Entity
public class PerfilTerminal extends PerfilUsuario {

	public PerfilTerminal() {
		super();
	}

	public PerfilTerminal(String nom, BuscadorPDIs repo) {
		super(nom);
		repositorio = repo;
	}

}