package ar.edu.utn.dds.g08.dominio;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class ParadaColectivo implements Serializable {
	@Id
	@ManyToOne
	public Parada parada;

	@Id
	@ManyToOne
	public Colectivo colectivo;

	public ParadaColectivo() {

	}

	public ParadaColectivo(Parada p, Colectivo c) {
		this.parada = p;
		this.colectivo = c;
	}

	public void setParada(Parada p) {
		this.parada = p;
	}

	public Parada getParada() {
		return this.parada;
	}

	public void setColectivo(Colectivo c) {
		this.colectivo = c;
	}

	public Colectivo getColectivo() {
		return this.colectivo;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ParadaColectivo))
			return false;

		ParadaColectivo pc = (ParadaColectivo) obj;

		// son iguales cuando tienen = direccion
		return this.parada.equals(pc.getParada())
				&& this.colectivo.equals(pc.getColectivo());

	}

	@Override
	public int hashCode() {
		return this.parada.hashCode() + this.colectivo.hashCode();
	}

}
