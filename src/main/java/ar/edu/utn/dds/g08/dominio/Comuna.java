package ar.edu.utn.dds.g08.dominio;

import ar.edu.utn.dds.g08.excepciones.NullParametro;
import ar.edu.utn.dds.g08.valueClasses.*;

import javax.persistence.*;

@Entity
public class Comuna {
	@Id
	@Column(name = "nro_comuna")
	protected String numero;

	@Embedded
	protected PolygonAdaptado radio;

	protected String zonasIncluidas;

	protected Comuna() {

	}

	public Comuna(String n, PolygonAdaptado r) {
		this();

		if (n == null)
			throw new NullParametro(
					"Ingrese valores no nulos para el N° de comuna");

		this.setNumero(n);
		this.setRadio(r);

	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNumero() {
		return numero;
	}

	public void setRadio(PolygonAdaptado radio) {
		this.radio = radio;
	}

	public PolygonAdaptado getRadio() {
		return this.radio;
	}

	public void agregarPunto(PointAdaptado unPunto) {
		this.radio.add(unPunto);
	}

	public boolean esCercano(PointAdaptado unPunto) {
		return this.radio.isInside(unPunto);
	}

	public String getZonasIncluidas() {
		return this.zonasIncluidas;
	}

	public void setZonasIncluidas(String zonas) {
		this.zonasIncluidas = zonas;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Comuna))
			return false;

		Comuna comuna = (Comuna) obj;

		return this.numero == comuna.getNumero();
	}

	@Override
	public int hashCode() {
		return this.numero.hashCode();
	}
	
	@Override
	public String toString(){
		return "Comuna" + " " + this.numero;
	}
}

