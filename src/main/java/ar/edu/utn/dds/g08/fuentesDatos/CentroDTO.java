package ar.edu.utn.dds.g08.fuentesDatos;

import java.util.List;

public class CentroDTO {
	private Integer nroComuna;
	private String zonas;
	private String director;
	private String domicilio;
	private String telefono;
	private List<ServicioDTO> servicios;

	public CentroDTO(int nro,String zon,String dir,String dom,String tel,List<ServicioDTO> serv){
		nroComuna = nro;
		zonas = zon;
		director = dir;
		domicilio = dom;
		telefono = tel;
		servicios = serv;
	}
	
	public void setNroComuna(int nro){
		this.nroComuna = nro;
	}
	
	public Integer getNroComuna(){
		return nroComuna;
	}

	public void setZonas(String zon){
		this.zonas = zon;
	}
	
	public String getZonas(){
		return zonas;
	}

	public void setDirector(String dir){
		this.director = dir;
	}
	
	public String getDirector(){
		return director;
	}

	public void setDomicilio(String dom){
		this.domicilio = dom;
	}
	
	public String getDomicilio(){
		return domicilio;
	}

	public void setTelefono(String tel){
		this.telefono = tel;
	}
	
	public String getTelefono(){
		return telefono;
	}

	public void setServicios(List<ServicioDTO> ser){
		this.servicios = ser;
	}
	
	public List<ServicioDTO> getServicios(){
		return servicios;
	}
}
