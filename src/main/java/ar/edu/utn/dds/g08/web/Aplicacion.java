package ar.edu.utn.dds.g08.web;

import javax.persistence.*;

import org.hibernate.Session;

import ar.edu.utn.dds.g08.busquedas.*;
import ar.edu.utn.dds.g08.datos.*;
import ar.edu.utn.dds.g08.dominio.*;
import ar.edu.utn.dds.g08.fuentesDatos.*;
import ar.edu.utn.dds.g08.perfilesUsuarios.PerfilAdministrador;
import ar.edu.utn.dds.g08.perfilesUsuarios.PerfilTerminal;
import ar.edu.utn.dds.g08.persistencia.*;

public class Aplicacion {

	public static void main(String[] args) {

		iniciarRepositorio();

		LoginController.init();
	}

	private static void iniciarRepositorio() {
		RepositorioPDI repositorio = new CreadorRepositorioPDI()
				.buildRepositorio();

		PerfilAdministrador adm = new PerfilAdministrador("administrador");
		adm.setContrasenia("passadmin");
		adm.setMail("administrador@utn.edu.ar");
		adm.setRepositorio(new BuscadorRepositorio(repositorio));

		PerfilTerminal terminal = new PerfilTerminal("terminal",
				new BuscadorRepositorio(repositorio));
		terminal.setContrasenia("passterminal");
		terminal.setMail("terminal@utn.edu.ar");

		Session sesion = SessionFactorySingleton.getInstance().openSession();
		EntityTransaction transaccion = sesion.beginTransaction();

		try {
			repositorio.stream().filter(p -> p.getClass() == Parada.class)
					.forEach(p -> sesion.persist(p));
			repositorio.stream().filter(p -> p.getClass() != Parada.class)
					.forEach(p -> sesion.persist(p));

			sesion.persist(adm);
			sesion.persist(terminal);

		} catch (Exception ex) {
			ex.printStackTrace();
			transaccion.rollback();
		}

		transaccion.commit();

		sesion.close();
	}
}
